#!/bin/bash
set -e
cur_dir=`dirname $0`

set -a; source ../../.env; set +a
sed -i "" "s|// signingConfig signingConfigs.release|signingConfig signingConfigs.release|" ../../android/app/build.gradle

echo "BUILDING ANDROID";

ANDROID_KEYSTORE_FILE='my-upload-key.keystore'
ANDROID_KEY_ALIAS='my-key-alias'

cd $cur_dir/../../android &&
./gradlew clean assembleRelease -PMYAPP_RELEASE_STORE_FILE=$ANDROID_KEYSTORE_FILE -PMYAPP_RELEASE_KEY_ALIAS=$ANDROID_KEY_ALIAS -PMYAPP_RELEASE_STORE_PASSWORD=$ANDROID_KEYSTORE_PASSWORD -PMYAPP_RELEASE_KEY_PASSWORD=$ANDROID_KEY_PASSWORD && cd ..

echo "APK will be present"