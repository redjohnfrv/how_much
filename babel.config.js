module.exports = {
  plugins: [
    'react-native-reanimated/plugin',
    '@babel/plugin-proposal-export-namespace-from',
    [
      'module:react-native-dotenv',
      {
        allowlist: null,
        allowUndefined: true,
        blacklist: null,
        blocklist: null,
        envName: 'APP_ENV',
        moduleName: '@env',
        path: '.env',
        safe: false,
        verbose: false,
        whitelist: null,
      },
    ],
    [
      'module-resolver',
      {
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        root: ['./src'],
      },
    ],
  ],
  presets: ['module:metro-react-native-babel-preset'],
};
