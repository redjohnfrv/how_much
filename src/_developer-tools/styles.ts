import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {},
});

export const light = StyleSheet.create({
  root: {
    ...styles.root,
  },
});

export const dark = StyleSheet.create({
  root: {
    ...styles.root,
  },
});
