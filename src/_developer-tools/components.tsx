import { View } from 'react-native';

import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

export const Components = () => {
  const theme = useAppTheme({ dark, light });

  return <View style={theme.root}></View>;
};
