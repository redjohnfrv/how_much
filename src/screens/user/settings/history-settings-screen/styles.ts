import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  buttons: {
    flex: 1,
    gap: 8,
    justifyContent: 'center',
  },
});

export const light = StyleSheet.create({
  buttons: {
    ...styles.buttons,
  },
});

export const dark = StyleSheet.create({
  buttons: {
    ...styles.buttons,
  },
});
