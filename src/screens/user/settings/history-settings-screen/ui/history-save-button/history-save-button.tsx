import React from 'react';
import RNFS, { mkdir, writeFile } from 'react-native-fs';
import { useToast } from 'react-native-toast-notifications';

import { useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';
import { useModalState } from 'resources/hooks';
import { Style } from 'resources/types';
import { Button } from 'ui';

import { ModalContent } from '../modal-content';

interface HistorySaveButtonProps {
  className?: Style;
}

export const HistorySaveButton = ({ className }: HistorySaveButtonProps) => {
  const history = useAppSelector(selectors.history.history);
  const toast = useToast();
  const { closeModal, isVisible, openModal } = useModalState({});

  const handleSaveHistory = async () => {
    try {
      const historyPath = `${RNFS.DocumentDirectoryPath}/history`;

      // creating catalog if it doesn't exist
      await mkdir(RNFS.DocumentDirectoryPath);

      await writeFile(historyPath, JSON.stringify(history), 'utf8');

      toast.show(`История сохранена в  ${historyPath}`, { type: 'success' });
    } catch (error) {
      toast.show(`Ошибка сохранения:  ${error}`, { type: 'error' });
    }
  };

  return (
    <Button.WithConfirmation
      content={<ModalContent text="Сохранить историю в локальный файл?" />}
      isModalVisible={isVisible}
      onClose={closeModal}
      onConfirm={handleSaveHistory}
      onPress={openModal}
      style={className}
      text="Сохранить историю"
      title="Сохранение истории"
      variant="positive"
    />
  );
};
