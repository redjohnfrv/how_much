import React from 'react';
import { useToast } from 'react-native-toast-notifications';

import { actions } from 'redux/actions';
import { useAppDispatch } from 'redux/hooks';
import { useModalState } from 'resources/hooks';
import { Style } from 'resources/types';
import { Button } from 'ui';

import { ModalContent } from '../modal-content';

interface HistoryClearButtonProps {
  className?: Style;
}

export const HistoryClearButton = ({ className }: HistoryClearButtonProps) => {
  const dispatch = useAppDispatch();

  const toast = useToast();
  const { closeModal, isVisible, openModal } = useModalState({});

  const handleClearHistory = () => {
    dispatch(actions.history.clearHistory());
    toast.show('История очищена!', { type: 'success' });
  };

  return (
    <Button.WithConfirmation
      content={<ModalContent text="Историю можно будет восстановить." />}
      isModalVisible={isVisible}
      onClose={closeModal}
      onConfirm={handleClearHistory}
      onPress={openModal}
      style={className}
      text="Очистка истории"
      title="Очистить историю?"
      variant="negative"
    />
  );
};
