import React from 'react';
import RNFS from 'react-native-fs';
import { useToast } from 'react-native-toast-notifications';

import { actions } from 'redux/actions';
import { useAppDispatch } from 'redux/hooks';
import { useModalState } from 'resources/hooks';
import { Style } from 'resources/types';
import { Button } from 'ui';

import { ModalContent } from '../modal-content';

interface HistoryRestoreProps {
  className?: Style;
}

export const HistoryRestoreButton = ({ className }: HistoryRestoreProps) => {
  const dispatch = useAppDispatch();
  const toast = useToast();
  const { closeModal, isVisible, openModal } = useModalState({});

  const handleRestoreHistory = () =>
    RNFS.readFile(RNFS.DocumentDirectoryPath + '/history', 'utf8')
      .then(async file => {
        const historyFromFile = await JSON.parse(file);

        dispatch(actions.history.restoreHistory({ history: historyFromFile }));
        toast.show(`История восстановлена из файла history.json`, {
          type: 'success',
        });
      })
      .catch(error => {
        toast.show(`Ошибка восстановления:  ${error}`, { type: 'error' });
      });

  return (
    <Button.WithConfirmation
      content={
        <ModalContent text="Восстановить историю из локального файла приложения?" />
      }
      isModalVisible={isVisible}
      onClose={closeModal}
      onConfirm={handleRestoreHistory}
      onPress={openModal}
      style={className}
      text="Восстановить историю"
      title="Восстановление истории"
      variant="positive"
    />
  );
};
