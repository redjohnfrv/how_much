import React from 'react';
import { Text } from 'react-native';

import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

interface ModalContentProps {
  text: string;
}

export const ModalContent = ({ text }: ModalContentProps) => {
  const theme = useAppTheme({ dark, light });

  return <Text style={theme.modalText}>{text}</Text>;
};
