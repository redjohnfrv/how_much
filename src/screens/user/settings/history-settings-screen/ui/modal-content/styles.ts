import { StyleSheet } from 'react-native';

import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  modalText: {
    ...typography.p3_bold_18,
  },
});

export const light = StyleSheet.create({
  modalText: {
    ...styles.modalText,
  },
});

export const dark = StyleSheet.create({
  modalText: {
    ...styles.modalText,
  },
});
