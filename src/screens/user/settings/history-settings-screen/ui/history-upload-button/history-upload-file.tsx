import React from 'react';
import DocumentPicker, {
  DocumentPickerResponse,
} from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import { useToast } from 'react-native-toast-notifications';

import { actions } from 'redux/actions';
import { useAppDispatch } from 'redux/hooks';
import { useModalState } from 'resources/hooks';
import { Style } from 'resources/types';
import { Button } from 'ui';

import { ModalContent } from '../modal-content';

interface HistoryUploadButtonProps {
  className?: Style;
}

export const HistoryUploadButton = ({
  className,
}: HistoryUploadButtonProps) => {
  const toast = useToast();
  const dispatch = useAppDispatch();
  const { closeModal, isVisible, openModal } = useModalState({});

  const handleUploadFile = async () => {
    try {
      const res: DocumentPickerResponse | DocumentPickerResponse[] =
        await DocumentPicker.pick({
          allowMultiSelection: false,
          type: [DocumentPicker.types.allFiles],
        });

      let fileUri = '';
      if (Array.isArray(res)) {
        // Обработка для массива файлов
        fileUri = res[0].uri;
      } else {
        // Обработка для одного файла
        fileUri = (res as DocumentPickerResponse).uri;
      }

      RNFS.readFile(fileUri, 'utf8')
        .then(content => {
          const historyFromFile = JSON.parse(content);
          const payload = {
            history: historyFromFile,
          };

          dispatch(actions.history.restoreHistory(payload));
          toast.show('История загружена!', { type: 'success' });
        })
        .catch(error => {
          toast.show(`Ошибка при чтении файла: ${error}`, { type: 'error' });
        });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        toast.show('Файл не выбран', { type: 'error' });
      } else {
        toast.show('Что-то пошло не так...', { type: 'error' });
      }
    }
  };

  return (
    <Button.WithConfirmation
      content={
        <ModalContent text="Загрузить историю из файла? Внимание! Это приведет к глубокой очистке истории и удалении истории по месяцам." />
      }
      isModalVisible={isVisible}
      onClose={closeModal}
      onConfirm={handleUploadFile}
      onPress={openModal}
      style={className}
      text="Загрузить историю"
      title="Загрузка истории"
      variant="negative"
    />
  );
};
