import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: 12,
    height: 50,
    justifyContent: 'center',
    width: 50,
  },
  content: {
    ...typography.p3_bold_18,
  },
  root: {
    marginLeft: 'auto',
  },
});

export const light = StyleSheet.create({
  button: {
    ...styles.root,
  },
  content: {
    ...styles.content,
  },
  pressed: {
    backgroundColor: colors.light.opacity_50,
  },
  root: {
    ...styles.root,
  },
});

export const dark = StyleSheet.create({
  button: {
    ...styles.root,
  },
  content: {
    ...styles.content,
  },
  pressed: {
    backgroundColor: colors.dark.opacity_50,
  },
  root: {
    ...styles.root,
  },
});
