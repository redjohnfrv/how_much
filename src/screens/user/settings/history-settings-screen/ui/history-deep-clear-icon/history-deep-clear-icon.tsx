import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';

import { actions } from 'redux/actions';
import { useAppDispatch } from 'redux/hooks';
import { useAppTheme } from 'resources/hooks';
import { TrashIcon } from 'resources/icons';
import { Modal } from 'ui';

import { dark, light } from './styles';

export const HistoryDeepClearIcon = () => {
  const theme = useAppTheme({ dark, light });
  const toast = useToast();
  const dispatch = useAppDispatch();
  const [isModalVisible, setModalVisible] = useState(false);

  const onClose = () => {
    setModalVisible(false);
  };

  const onPress = () => {
    setModalVisible(true);
  };

  const onConfirm = () => {
    try {
      dispatch(actions.history.deepClearHistory());
      toast.show('История очищена! ', { type: 'success' });
    } catch (error) {
      toast.show(`Ошибка сохранения:  ${error}`, { type: 'error' });
    } finally {
      setModalVisible(false);
    }
  };

  return (
    <View style={theme.root}>
      <Pressable
        onPress={onPress}
        style={({ pressed }) => [theme.button, pressed && theme.pressed]}>
        <TrashIcon />
      </Pressable>

      <Modal.Default
        onClose={onClose}
        onConfirm={onConfirm}
        title="Глубокая очистка"
        visible={isModalVisible}>
        <Text style={theme.content}>
          Глубокая очистка истории удалит все итоговые суммы, в том числе по
          месяцам. Продолжить?
        </Text>
      </Modal.Default>
    </View>
  );
};
