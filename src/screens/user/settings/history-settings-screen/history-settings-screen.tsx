import React, { FC } from 'react';
import { View } from 'react-native';

import { useAppTheme } from 'resources/hooks';

import { UserLayout } from '../../ui';
import { dark, light } from './styles';
import { HistorySettingsScreenProps } from './types';
import {
  HistoryClearButton,
  HistoryDeepClearIcon,
  HistoryRestoreButton,
  HistorySaveButton,
  HistoryUploadButton,
} from './ui';

export const HistorySettingsScreen: FC<HistorySettingsScreenProps> = () => {
  const theme = useAppTheme({ dark, light });
  return (
    <UserLayout title="Настройки истории" withBackButton>
      <View style={theme.buttons}>
        <HistorySaveButton />
        <HistoryRestoreButton />
        <HistoryUploadButton />
        <HistoryClearButton />
      </View>

      <HistoryDeepClearIcon />
    </UserLayout>
  );
};
