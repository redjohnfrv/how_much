import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  arrowsWrapper: {
    gap: 16,
  },
  button: {
    marginBottom: 8,
  },
  chapter: {
    paddingBottom: 24,
  },
  footer: {
    marginTop: 'auto',
  },
  title: {
    marginBottom: 24,
    width: '100%',
    ...typography.h1_bold_24,
  },
  toggleContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 16,
  },
  toggleLabel: {
    ...typography.p3_bold_18,
  },
});

export const light = StyleSheet.create({
  arrowsWrapper: {
    ...styles.arrowsWrapper,
  },
  button: {
    ...styles.button,
  },
  chapter: {
    ...styles.chapter,
  },
  footer: {
    ...styles.footer,
  },
  title: {
    ...styles.title,
    color: colors.light.primary[1],
  },
  toggleContainer: {
    ...styles.toggleContainer,
  },
  toggleLabel: {
    ...styles.toggleLabel,
    color: colors.light.primary[1],
  },
});

export const dark = StyleSheet.create({
  arrowsWrapper: {
    ...styles.arrowsWrapper,
  },
  button: {
    ...styles.button,
  },
  chapter: {
    ...styles.chapter,
  },
  footer: {
    ...styles.footer,
  },
  title: {
    ...styles.title,
    color: colors.dark.primary[1],
  },
  toggleContainer: {
    ...styles.toggleContainer,
  },
  toggleLabel: {
    ...styles.toggleLabel,
    color: colors.dark.primary[1],
  },
});
