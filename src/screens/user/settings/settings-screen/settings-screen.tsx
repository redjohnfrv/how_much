import React, { FC, useState } from 'react';
import { Text, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { APP_ROUTES } from 'navigation/routes';
import {
  SettingsStackNavigationProp,
  SettingsStackParamsList,
} from 'navigation/user/stacks/settings/settings-stack';
import { actions } from 'redux/actions';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';
import { useAppTheme, useAppThemeType } from 'resources/hooks';
import { ThemeType } from 'resources/styles/themes/types';
import { Button, Toggle } from 'ui';

import { UserLayout } from '../../ui';
import { dark, light } from './styles';
import { SettingsScreenProps } from './types';

export const SettingsScreen: FC<SettingsScreenProps> = () => {
  const dispatch = useAppDispatch();
  const theme = useAppTheme({ dark, light });
  const navigation = useNavigation<SettingsStackNavigationProp>();
  const themeType = useAppThemeType();
  const themeTypeAdapter: Record<ThemeType, string> = {
    dark: 'Тёмная тема',
    light: 'Светлая тема',
    system: 'Как в системе',
  };

  const appTheme = useAppSelector(selectors.settings.theme);
  const [isSystemTheme, setSystemTheme] = useState(appTheme === 'system');

  const handleSystemThemeToggle = () => {
    if (isSystemTheme) {
      dispatch(actions.settings.changeColorTheme(themeType));
      setSystemTheme(false);
    } else {
      dispatch(actions.settings.changeColorTheme('system'));
      setSystemTheme(true);
    }
  };

  const handleAppThemeToggle = () => {
    if (appTheme === 'light') {
      dispatch(actions.settings.changeColorTheme('dark'));
    } else {
      dispatch(actions.settings.changeColorTheme('light'));
    }
  };

  const handleNavigate = (route: keyof SettingsStackParamsList) => {
    navigation.navigate(route);
  };

  const handleLogout = () => {
    dispatch(actions.auth.signOut());
  };

  return (
    <UserLayout title="Настройки">
      <View style={theme.chapter}>
        <Text style={theme.title}>Тема приложения</Text>

        <View style={theme.toggleContainer}>
          <Text style={theme.toggleLabel}>{themeTypeAdapter.system}</Text>
          <Toggle checked={isSystemTheme} onClick={handleSystemThemeToggle} />
        </View>

        <View style={theme.toggleContainer}>
          <Text style={theme.toggleLabel}>{themeTypeAdapter[themeType]}</Text>

          <Toggle
            checked={appTheme === 'dark'}
            disabled={isSystemTheme}
            onClick={handleAppThemeToggle}
          />
        </View>
      </View>

      <View style={theme.arrowsWrapper}>
        <Button.NavigationArrow
          onPressed={() => handleNavigate(APP_ROUTES.HISTORY_SETTINGS_SCREEN)}
          title="К настройкам истории"
        />

        <Button.NavigationArrow
          onPressed={() => handleNavigate(APP_ROUTES.RELEASE_NOTES_SCREEN)}
          title="К release notes"
        />
      </View>

      <View style={theme.footer}>
        <Button.Default
          onPress={handleLogout}
          text="Выйти из приложения"
          variant="secondary"
        />
      </View>
    </UserLayout>
  );
};
