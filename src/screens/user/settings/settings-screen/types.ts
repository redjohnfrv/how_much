import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { APP_ROUTES } from 'navigation/routes';
import { SettingsStoriesStackParamListExtended } from 'navigation/user/stacks/settings/settings-stack';

export interface SettingsScreenProps {
  navigation: NativeStackNavigationProp<
    SettingsStoriesStackParamListExtended,
    typeof APP_ROUTES.SETTINGS_SCREEN
  >;
  route: RouteProp<
    SettingsStoriesStackParamListExtended,
    typeof APP_ROUTES.SETTINGS_SCREEN
  >;
}
