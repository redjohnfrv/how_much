import { FC } from 'react';
import { FlatList, View } from 'react-native';

import { UserLayout } from '../../ui';
import { release_1_0, release_1_1, release_1_2, release_1_3 } from './releases';
import { styles } from './styles';
import { ReleaseNotesScreenProps } from './types';
import { ReleaseList } from './ui/release-list';

const releases = [release_1_0, release_1_1, release_1_2, release_1_3];

export const ReleaseNotesScreen: FC<ReleaseNotesScreenProps> = () => {
  return (
    <UserLayout title="Новости релизов" withBackButton>
      <FlatList
        data={releases}
        ItemSeparatorComponent={() => <View style={styles.root} />}
        keyExtractor={item => item.title}
        renderItem={({ item }) => (
          <ReleaseList data={item.notes} releaseName={item.title} />
        )}
      />
    </UserLayout>
  );
};
