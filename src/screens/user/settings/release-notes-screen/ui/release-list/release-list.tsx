import { FlatList, Text } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Surface } from 'ui';

import { dark, light } from './styles';

interface ReleaseListProps {
  data: string[];
  releaseName: string;
}

export const ReleaseList = ({ data, releaseName }: ReleaseListProps) => {
  const theme = useAppTheme({ dark, light });

  return (
    <Surface.Default>
      <Text style={theme.title}>{releaseName}</Text>
      <FlatList
        data={data}
        keyExtractor={item => item}
        renderItem={({ item }) => (
          <Text style={theme.text}>&#8226; {item}</Text>
        )}
      />
    </Surface.Default>
  );
};
