import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  text: {
    ...typography.p1_regular_16,
  },
  title: {
    marginBottom: 12,
    ...typography.p3_bold_18,
  },
});

export const light = StyleSheet.create({
  text: {
    ...styles.text,
    color: colors.light.primary[2],
  },
  title: {
    ...styles.title,
    color: colors.light.primary[2],
  },
});

export const dark = StyleSheet.create({
  text: {
    ...styles.text,
    color: colors.dark.primary[1],
  },
  title: {
    ...styles.title,
    color: colors.dark.primary[1],
  },
});
