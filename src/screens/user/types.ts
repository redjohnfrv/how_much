import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import {
  HistoryStackParamsList,
  HistoryStoriesStackParamListExtended,
} from 'navigation/user/stacks/history/history-stack';

export type HistoryScreenNavigationProp = NativeStackNavigationProp<
  HistoryStoriesStackParamListExtended,
  keyof HistoryStackParamsList
>;
