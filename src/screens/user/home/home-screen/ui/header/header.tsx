import React from 'react';
import { Image, ImageStyle, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { APP_ROUTES } from 'navigation/routes';
import { BottomStackNavigationProp } from 'navigation/user/stacks';
import { useAppTheme } from 'resources/hooks';
import logo from 'resources/images/logo/how-much-logo.png';
import { Button } from 'ui';

import { dark, light } from './styles';

export const Header = () => {
  const theme = useAppTheme({ dark, light });
  const navigation = useNavigation<BottomStackNavigationProp>();

  const handleHistoryNavigate = () => {
    navigation.navigate(APP_ROUTES.HISTORY_STACK);
  };
  return (
    <View style={theme.header}>
      <View style={theme.logo}>
        <Image source={logo} style={theme.image as ImageStyle} />
        <Button.NavigationArrow
          onPressed={handleHistoryNavigate}
          title="Посмотреть историю трат"
        />
      </View>
    </View>
  );
};
