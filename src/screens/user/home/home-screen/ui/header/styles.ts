import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  header: {
    flex: 1,
    marginBottom: 30,
  },
  image: {
    height: 135,
    width: 135,
  },
  logo: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export const light = StyleSheet.create({
  header: {
    ...styles.header,
  },
  image: {
    ...styles.image,
  },
  logo: {
    ...styles.logo,
  },
});

export const dark = StyleSheet.create({
  header: {
    ...styles.header,
  },
  image: {
    ...styles.image,
  },
  logo: {
    ...styles.logo,
  },
});
