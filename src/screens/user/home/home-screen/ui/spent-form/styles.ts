import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  button: {
    marginTop: 'auto',
  },
  inputWrap: {
    marginBottom: 20,
  },
});

export const light = StyleSheet.create({
  button: {
    ...styles.button,
  },
  inputWrap: {
    ...styles.inputWrap,
  },
});

export const dark = StyleSheet.create({
  button: {
    ...styles.button,
  },
  inputWrap: {
    ...styles.inputWrap,
  },
});
