import React, { useCallback, useState } from 'react';
import { Field, Form } from 'react-final-form';
import { useToast } from 'react-native-toast-notifications';

import { useFocusEffect } from '@react-navigation/native';
import { FormApi } from 'final-form';
import moment from 'moment/moment';
import { actions } from 'redux/actions';
import { useAppDispatch } from 'redux/hooks';
import { SpentTags } from 'redux/reducers/history/types';
import { DATE_FORMAT, DATE_INPUT_LENGTH } from 'resources/constants';
import { useAppTheme, useModalState } from 'resources/hooks';
import { utils } from 'resources/utils';
import { regex } from 'resources/utils/regex';
import { Tags } from 'screens/user/ui';
import { Button, Input } from 'ui';

import { dark, light } from './styles';

type FormValues = {
  date?: string;
  name: string;
  value: string;
};

export const SpentForm = () => {
  const theme = useAppTheme({ dark, light });
  const toast = useToast();
  const dispatch = useAppDispatch();
  const { getNormalizedDateValue, getNormalizedValue, validator } = utils;
  const { closeModal, isVisible, openModal } = useModalState({});
  const [dateError, setDateError] = useState<string | undefined>(undefined);
  const [tagValue, setTagValue] = useState<SpentTags>(SpentTags.ANYTHING);

  const onSubmit = (values: FormValues, form: FormApi<FormValues>) => {
    const normalizedDateValue = getNormalizedDateValue(values?.date);
    setDateError(validator.isDate(normalizedDateValue));

    if (!values.date) {
      values.date = moment().format(DATE_FORMAT);
    }

    try {
      let date = values.date;

      if (values.date.indexOf('.') < 0) {
        date = date.replace(regex.divideByTwo, '$1.$2.$3');
      }

      dispatch(
        actions.history.updateHistory({
          ...(values as Required<FormValues>),
          date,
          tag: tagValue as keyof typeof SpentTags,
        }),
      );
      toast.show(`Потрачено ${values?.value} рублей!`, { type: 'success' });
    } catch {
      toast.show('Что-то пошло не так...', { type: 'error' });
    } finally {
      form.restart();
    }
  };

  useFocusEffect(
    useCallback(() => {
      return () => {
        setDateError(undefined);
      };
    }, []),
  );

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => {
        return (
          <>
            <Field name="date">
              {({ input }) => (
                <Input.Default
                  error={dateError}
                  keyboardType="decimal-pad"
                  maxLength={DATE_INPUT_LENGTH}
                  onChange={value => {
                    setDateError(undefined);
                    input.onChange(value);
                  }}
                  placeholder={`Введите дату ${DATE_FORMAT}`}
                  style={theme.inputWrap}
                  value={getNormalizedDateValue(input.value)}
                />
              )}
            </Field>
            <Field name="name" validate={validator.isEmptyString}>
              {({ input, meta }) => (
                <Input.Default
                  error={meta.touched && meta.error}
                  onChange={input.onChange}
                  placeholder="На что тратим?"
                  style={theme.inputWrap}
                  value={input.value}
                />
              )}
            </Field>
            <Field name="value" validate={validator.isEmptyString}>
              {({ input, meta }) => (
                <Input.Default
                  error={meta.touched && meta.error}
                  keyboardType="decimal-pad"
                  onChange={input.onChange}
                  placeholder="Сумма"
                  style={theme.inputWrap}
                  value={getNormalizedValue(input.value)}
                />
              )}
            </Field>

            <Button.WithConfirmation
              content={
                <Tags activeTags={[tagValue]} onTagPress={setTagValue} />
              }
              isModalVisible={isVisible}
              onClose={closeModal}
              onConfirm={handleSubmit}
              onPress={openModal}
              pointerEvents="box-only"
              style={theme.button}
              text="Потратить!"
              title="Подтвердить трату?"
              variant="primary"
            />
          </>
        );
      }}
    />
  );
};
