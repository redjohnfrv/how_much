import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  content: {
    flex: 2,
  },
});

export const light = StyleSheet.create({
  content: {
    ...styles.content,
  },
});

export const dark = StyleSheet.create({
  content: {
    ...styles.content,
  },
});
