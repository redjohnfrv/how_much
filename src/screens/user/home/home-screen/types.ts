import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { APP_ROUTES } from 'navigation/routes';
import { HomeStoriesStackParamListExtended } from 'navigation/user/stacks/home/home-stack';

export interface HomeScreenProps {
  navigation: NativeStackNavigationProp<
    HomeStoriesStackParamListExtended,
    typeof APP_ROUTES.HOME_SCREEN
  >;
  route: RouteProp<
    HomeStoriesStackParamListExtended,
    typeof APP_ROUTES.HOME_SCREEN
  >;
}
