import React, { FC } from 'react';
import { View } from 'react-native';

import { useAppTheme } from 'resources/hooks';

import { UserLayout } from '../../ui';
import { dark, light } from './styles';
import { HomeScreenProps } from './types';
import { Header, SpentForm } from './ui';

export const HomeScreen: FC<HomeScreenProps> = () => {
  const theme = useAppTheme({ dark, light });

  return (
    <UserLayout title="SO HOW MUCH?">
      <Header />

      <View style={theme.content}>
        <SpentForm />
      </View>
    </UserLayout>
  );
};
