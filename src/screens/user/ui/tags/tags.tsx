import { View } from 'react-native';

import { SpentTags, spentTagsAdapter } from 'redux/reducers/history/types';
import { useAppTheme } from 'resources/hooks';
import { Tag } from 'ui';

import { dark, light } from './styles';

export type Tag = {
  label: string;
  value: SpentTags;
};

const tags = Object.entries(spentTagsAdapter).reduce((acc: Tag[], item) => {
  acc.push({
    label: Object.values(item)[1],
    value: Object.values(item)[0] as SpentTags,
  });

  return acc;
}, []);

interface TagsProps {
  activeTags: string[] | undefined;
  onTagPress: (tag: SpentTags) => void;
}

export const Tags = ({ activeTags, onTagPress }: TagsProps) => {
  const theme = useAppTheme({ dark, light });

  return (
    <View style={theme.root}>
      {tags.map(tag => {
        const activeTag = activeTags?.includes(tag.value);

        return (
          <Tag.Default
            isActive={Boolean(activeTag)}
            key={tag.value}
            onPress={() => onTagPress(tag.value)}
            tag={tag.label}
          />
        );
      })}
    </View>
  );
};
