import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: 12,
    marginTop: 8,
  },
});

export const light = {
  root: {
    ...styles.root,
  },
};

export const dark = {
  root: {
    ...styles.root,
  },
};
