import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

import { Z_INDEX_VARIABLES } from '../../../../resources/styles/z-index';

export const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    justifyContent: 'center',
    marginBottom: 24,
    position: 'relative',
    width: '100%',
  },
  iconContainer: {
    alignItems: 'center',
    borderRadius: 24,
    height: 45,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    top: 5,
    width: 45,
    zIndex: Z_INDEX_VARIABLES.zIndexDecorActiveEl,
  },
  root: {
    flex: 1,
    paddingBottom: 86,
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  title: {
    ...typography.h1_bold_24,
    textAlign: 'center',
    width: '100%',
  },
});

export const light = StyleSheet.create({
  header: {
    ...styles.header,
  },
  icon: {
    backgroundColor: colors.light.primary[2],
  },
  iconContainer: {
    ...styles.iconContainer,
    backgroundColor: colors.light.grayscale[1],
  },
  iconContainerPressed: {
    ...styles.iconContainer,
    backgroundColor: colors.light.grayscale[4],
  },
  root: {
    ...styles.root,
    backgroundColor: colors.light.background[5],
  },
  title: {
    ...styles.title,
    color: colors.light.primary[2],
  },
});

export const dark = StyleSheet.create({
  header: {
    ...styles.header,
  },
  icon: {
    backgroundColor: colors.dark.primary[1],
  },
  iconContainer: {
    ...styles.iconContainer,
    backgroundColor: colors.dark.grayscale[0],
  },
  iconContainerPressed: {
    ...styles.iconContainer,
    backgroundColor: colors.dark.grayscale[7],
  },
  root: {
    ...styles.root,
    backgroundColor: colors.dark.background[3],
  },
  title: {
    ...styles.title,
    color: colors.dark.primary[1],
  },
});
