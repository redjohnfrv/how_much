import { FC, ReactNode } from 'react';
import { Pressable, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { useNavigation } from '@react-navigation/native';
import { useAppTheme } from 'resources/hooks';
import { BackIcon } from 'resources/icons';

import { dark, light } from './styles';

interface UserLayoutProps {
  children: ReactNode;
  title: string;
  withBackButton?: boolean;
}

export const UserLayout: FC<UserLayoutProps> = ({
  children,
  title,
  withBackButton,
}) => {
  const theme = useAppTheme({ dark, light });
  const navigation = useNavigation();

  const handleBackPress = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={theme.root}>
      <View style={theme.header}>
        {withBackButton && (
          <Pressable
            onPress={handleBackPress}
            style={({ pressed }) =>
              pressed ? theme.iconContainerPressed : theme.iconContainer
            }>
            <BackIcon
              color={theme.icon.backgroundColor}
              height={32}
              width={32}
            />
          </Pressable>
        )}
        <Text style={theme.title}>{title}</Text>
      </View>
      {children}
    </SafeAreaView>
  );
};
