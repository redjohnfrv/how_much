import { useMemo } from 'react';

import moment from 'moment/moment';
import { useAppSelector } from 'redux/hooks';
import { SpentTags } from 'redux/reducers/history/types';
import { selectors } from 'redux/selectors';
import { monthToNumAdapter, numToMonthAdapter } from 'resources/constants';
import { utils } from 'resources/utils';

/**
 * get MM.YY from DD.MM.YY format: 01.01.21 => 01.21
 * @param date
 */
const getMonthAndYearByDate = (date: string) => date.slice(3);
/**
 * get MM.YY from totals key object: январь 21 => 01.21
 * @param key
 */
const getMonthAndYearByKey = (key?: string) => {
  if (key) {
    const year = key.split(' ')[1];
    const month = key.split(' ')[0];

    return `${monthToNumAdapter[month]}.${year}`;
  }
};

export const useFilteredData = (
  tags: SpentTags[] | undefined,
  dateKey?: string,
) => {
  const { getTargetMonth, sortHistoryByDate } = utils;
  const history = useAppSelector(selectors.history.history);
  const totals = useAppSelector(selectors.history.prevTotal);

  const currentMonth = moment().format('MM');
  const currentDate = moment().format('MM.YY');
  const currentMonthName = numToMonthAdapter[currentMonth];

  // when user selected history by some month from totals screen
  const selectedDate = getMonthAndYearByKey(dateKey);

  const sum = getTargetMonth(totals, currentMonthName);

  const { filteredHistory, filteredSum } = useMemo(() => {
    const historyByTags = history?.filter(item => {
      if (item.tag) {
        if (tags?.includes(item.tag as SpentTags)) {
          return item;
        }
      }
    });

    const targetMonthHistory = (tags?.length ? historyByTags : history)?.filter(
      item => {
        // if user selected some month from totals screen
        if (dateKey) {
          return getMonthAndYearByDate(item.date) === selectedDate;
        } else {
          return getMonthAndYearByDate(item.date) === currentDate;
        }
      },
    );

    const sortedTargetMonthHistory = sortHistoryByDate(targetMonthHistory);

    const sumByTags = sortedTargetMonthHistory?.reduce((acc, next) => {
      acc += Number(next.value);

      return acc;
    }, 0);

    return {
      filteredHistory: sortedTargetMonthHistory,
      filteredSum: tags?.length ? String(sumByTags) : sum,
    };
  }, [
    currentDate,
    dateKey,
    history,
    selectedDate,
    sortHistoryByDate,
    sum,
    tags,
  ]);

  return {
    filteredHistory,
    filteredSum,
  };
};
