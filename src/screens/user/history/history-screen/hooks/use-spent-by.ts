import { useMemo, useState } from 'react';

import { useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';

export enum SPENT_BY {
  month = 'month',
  total = 'total',
}

export const useSpentBy = () => {
  const [spentBy, setSpentBy] = useState<SPENT_BY>(SPENT_BY.month);
  const isSpentByMonth = spentBy === SPENT_BY.month;

  const prevTotals = useAppSelector(selectors.history.prevTotal);
  const prevTotalsData = Object.entries(prevTotals).map(([key, value]) => ({
    [key]: value,
  }));

  const toggleLabel = useMemo(
    () => ({
      [SPENT_BY.month]: 'За текущий месяц',
      [SPENT_BY.total]: 'Общая по месяцам',
    }),
    [],
  );

  const handleSpentBySwitch = () => {
    setSpentBy(prev =>
      prev === SPENT_BY.month ? SPENT_BY.total : SPENT_BY.month,
    );
  };

  return useMemo(
    () => ({
      handleSpentBySwitch,
      isSpentByMonth,
      prevTotalsData,
      toggleChecked: spentBy === SPENT_BY.month,
      toggleLabel: toggleLabel[spentBy],
    }),
    [isSpentByMonth, prevTotalsData, spentBy, toggleLabel],
  );
};
