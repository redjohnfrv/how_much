import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { APP_ROUTES } from 'navigation/routes';
import { HistoryStoriesStackParamListExtended } from 'navigation/user/stacks/history/history-stack';

export interface HistoryScreenProps {
  navigation: NativeStackNavigationProp<
    HistoryStoriesStackParamListExtended,
    typeof APP_ROUTES.HISTORY_SCREEN
  >;
  route: RouteProp<
    HistoryStoriesStackParamListExtended,
    typeof APP_ROUTES.HISTORY_SCREEN
  >;
}
