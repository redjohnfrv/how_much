import React, { useState } from 'react';
import { FlatList, ListRenderItemInfo, View } from 'react-native';

import {
  HistoryItem,
  HistoryTotalItem,
  SpentTags,
} from 'redux/reducers/history/types';

import { UserLayout } from '../../ui';
import { useFilteredData } from '../hooks';
import { styles } from '../styles';
import { Filters, HistoryRenderItem } from '../ui';
import { useSpentBy } from './hooks';
import { Header } from './ui';

export const HistoryScreen = () => {
  const [tags, setTags] = useState<SpentTags[] | undefined>(undefined);
  const { filteredHistory, filteredSum } = useFilteredData(tags);
  const {
    handleSpentBySwitch,
    isSpentByMonth,
    prevTotalsData,
    toggleChecked,
    toggleLabel,
  } = useSpentBy();

  return (
    <UserLayout title="История трат">
      <Header
        checked={toggleChecked}
        label={toggleLabel}
        onToggle={handleSpentBySwitch}
        sum={filteredSum || ''}
      />

      {isSpentByMonth ? (
        <FlatList
          contentContainerStyle={{ paddingVertical: 20 }}
          data={filteredHistory?.reverse()}
          ItemSeparatorComponent={ItemSeparator}
          keyExtractor={keyExtractorByMonth}
          renderItem={RenderItemByMonth}
        />
      ) : (
        <FlatList
          contentContainerStyle={{ paddingVertical: 20 }}
          data={prevTotalsData}
          ItemSeparatorComponent={ItemSeparator}
          keyExtractor={keyExtractorByTotal}
          renderItem={RenderItemByTotal}
        />
      )}

      <Filters activeFilters={tags} setFilters={setTags} />
    </UserLayout>
  );
};

const RenderItemByMonth = ({ item }: ListRenderItemInfo<HistoryItem>) => (
  <HistoryRenderItem.ByMonth item={item} />
);

const keyExtractorByMonth = (item: HistoryItem) =>
  item.name + item.date + item.value;

const RenderItemByTotal = ({ item }: ListRenderItemInfo<HistoryTotalItem>) => (
  <HistoryRenderItem.ByTotal item={item} />
);

const keyExtractorByTotal = (item: HistoryTotalItem) => {
  return Object.keys(item)[0];
};

const ItemSeparator = () => <View style={styles.separator} />;
