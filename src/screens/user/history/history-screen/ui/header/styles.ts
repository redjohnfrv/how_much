import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  headerSum: {
    ...typography.h1_bold_32,
  },
  headerTitle: {
    marginBottom: 12,
    ...typography.p3_bold_18,
  },
  toggleContainer: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    gap: 16,
    marginBottom: 12,
  },
  toggleLabel: {
    ...typography.p1_regular_16,
  },
});

export const light = StyleSheet.create({
  headerSum: {
    ...styles.headerSum,
    color: colors.light.additional.success,
  },
  headerTitle: {
    ...styles.headerTitle,
    color: colors.light.primary[1],
  },
  toggleContainer: {
    ...styles.toggleContainer,
  },
  toggleLabel: {
    ...styles.toggleLabel,
    color: colors.light.primary[1],
  },
});

export const dark = StyleSheet.create({
  headerSum: {
    ...styles.headerSum,
    color: colors.dark.additional.success,
  },
  headerTitle: {
    ...styles.headerTitle,
    color: colors.dark.primary[1],
  },
  toggleContainer: {
    ...styles.toggleContainer,
  },
  toggleLabel: {
    ...styles.toggleLabel,
    color: colors.dark.primary[1],
  },
});
