import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import { Text, View } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Surface, Toggle } from 'ui';

import { dark, light } from './styles';

interface HeaderProps {
  checked: boolean;
  label: string;
  onToggle: () => void;
  sum: string;
}

export const Header: FC<HeaderProps> = ({ checked, label, onToggle, sum }) => {
  const theme = useAppTheme({ dark, light });

  return (
    <Surface.Default>
      <View style={theme.toggleContainer}>
        <Text style={theme.toggleLabel}>{label}</Text>
        <Toggle checked={checked} onClick={onToggle} />
      </View>

      {checked && (
        <>
          <Text style={theme.headerTitle}>Всего потрачено:</Text>
          <Text style={theme.headerSum}>{`${sum || 0} руб.`}</Text>
        </>
      )}
    </Surface.Default>
  );
};
