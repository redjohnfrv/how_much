import React, { FC } from 'react';
import { Text, View } from 'react-native';

import { NavigationProp, useNavigation } from '@react-navigation/native';
import { APP_ROUTES } from 'navigation/routes';
import { HistoryStackParamsList } from 'navigation/user/stacks/history/history-stack';
import { HistoryTotalItem } from 'redux/reducers/history/types';
import { useAppTheme } from 'resources/hooks';
import { Button } from 'ui';

import { dark, light } from './styles';

let month: string;
let sum: string;

interface ByTotalProps {
  item: HistoryTotalItem;
}

export const ByTotal: FC<ByTotalProps> = ({ item }) => {
  const theme = useAppTheme({ dark, light });
  const navigation = useNavigation<NavigationProp<HistoryStackParamsList>>();

  let historyDate: string;

  for (const key in item) {
    if (item.hasOwnProperty(key)) {
      historyDate = key;

      const totalItem = item[key];
      month = key;
      sum = totalItem;
    }
  }

  return (
    <View style={theme.root}>
      <Text numberOfLines={1} style={theme.name}>
        {month}
      </Text>
      <Text style={theme.value}>{sum} руб.</Text>
      <Button.NavigationArrow
        onPressed={() =>
          navigation.navigate(APP_ROUTES.HISTORY_BY_MONTH_SCREEN, {
            params: historyDate,
          })
        }
        title="Посмотреть подробную историю"
      />
    </View>
  );
};
