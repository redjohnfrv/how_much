import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  date: {
    textAlign: 'center',
    width: '100%',
    ...typography.p4_regular_24,
  },
  name: {
    width: '100%',
    ...typography.h1_bold_24,
  },
  root: {
    borderBottomWidth: 1.5,
    paddingBottom: 8,
    width: '100%',
  },
  value: {
    width: '100%',
    ...typography.p5_regular_36,
  },
});

export const light = StyleSheet.create({
  date: {
    ...styles.date,
    color: colors.light.grayscale[6],
  },
  name: {
    ...styles.name,
    color: colors.light.primary[3],
  },
  root: {
    ...styles.root,
    borderBottomColor: colors.light.primary[3],
  },
  value: {
    ...styles.value,
    color: colors.light.additional.warning_error,
  },
});

export const dark = StyleSheet.create({
  date: {
    ...styles.date,
    color: colors.dark.grayscale[5],
  },
  name: {
    ...styles.name,
    color: colors.dark.primary[1],
  },
  root: {
    ...styles.root,
    borderBottomColor: colors.dark.primary[3],
  },
  value: {
    ...styles.value,
    color: colors.dark.additional.warning_error,
  },
});
