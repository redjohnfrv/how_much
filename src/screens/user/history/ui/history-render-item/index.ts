import { ByMonth } from './by-month';
import { ByTotal } from './by-total';

export const HistoryRenderItem = {
  ByMonth,
  ByTotal,
};
