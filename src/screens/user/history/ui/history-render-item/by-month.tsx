import { FC } from 'react';
import { Text, View } from 'react-native';

import { HistoryItem } from 'redux/reducers/history/types';
import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

interface ByMonthProps {
  item: HistoryItem;
}

export const ByMonth: FC<ByMonthProps> = ({ item }) => {
  const theme = useAppTheme({ dark, light });

  return (
    <View style={theme.root}>
      <Text style={theme.date}>{item.date}</Text>
      <Text numberOfLines={1} style={theme.name}>
        {item.name}
      </Text>
      <Text style={theme.value}>{item.value} руб.</Text>
    </View>
  );
};
