import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { Z_INDEX_VARIABLES } from 'resources/styles/z-index';

export const styles = StyleSheet.create({
  iconContainer: {
    alignItems: 'center',
    borderRadius: 30,
    height: 61,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    top: 5,
    width: 61,
    zIndex: Z_INDEX_VARIABLES.zIndexDecorActiveEl,
  },
  root: {
    bottom: 136,
    position: 'absolute',
    right: 66,
  },
});

export const light = {
  icon: {
    backgroundColor: colors.light.primary[2],
  },
  iconContainer: {
    ...styles.iconContainer,
    backgroundColor: colors.light.grayscale[1],
  },
  iconContainerPressed: {
    ...styles.iconContainer,
    backgroundColor: colors.light.grayscale[4],
  },
  root: {
    ...styles.root,
  },
};

export const dark = {
  icon: {
    backgroundColor: colors.dark.primary[1],
  },
  iconContainer: {
    ...styles.iconContainer,
    backgroundColor: colors.dark.grayscale[0],
  },
  iconContainerPressed: {
    ...styles.iconContainer,
    backgroundColor: colors.dark.grayscale[7],
  },
  root: {
    ...styles.root,
  },
};
