import React, { Dispatch } from 'react';
import { Pressable, View } from 'react-native';

import { SpentTags } from 'redux/reducers/history/types';
import { useAppTheme, useModalState } from 'resources/hooks';
import { FiltersIcon } from 'resources/icons';
import { Tags } from 'screens/user/ui';
import { Modal } from 'ui';

import { dark, light } from './styles';

interface FiltersProps {
  activeFilters: SpentTags[] | undefined;
  setFilters: Dispatch<React.SetStateAction<SpentTags[] | undefined>>;
}

export const Filters = ({ activeFilters, setFilters }: FiltersProps) => {
  const theme = useAppTheme({ dark, light });
  const { closeModal, isVisible, openModal } = useModalState({});

  const onTagPress = (tag: SpentTags) => {
    setFilters(prev => {
      const result = prev?.length ? [...prev] : [];

      if (result.includes(tag)) {
        return result.filter(t => t !== tag);
      } else {
        result.push(tag);
      }

      return result;
    });
  };

  return (
    <View style={theme.root}>
      <Pressable
        onPress={openModal}
        style={({ pressed }) =>
          pressed ? theme.iconContainerPressed : theme.iconContainer
        }>
        <FiltersIcon
          color={theme.icon.backgroundColor}
          height={32}
          width={32}
        />
      </Pressable>

      <Modal.Default
        confirmButtonText="Закрыть"
        onConfirm={closeModal}
        title="Выбрите фильтры:"
        visible={isVisible}>
        <Tags activeTags={activeFilters} onTagPress={onTagPress} />
      </Modal.Default>
    </View>
  );
};
