import React, { useState } from 'react';
import { FlatList, ListRenderItemInfo, View } from 'react-native';

import { HistoryItem, SpentTags } from 'redux/reducers/history/types';

import { UserLayout } from '../../ui';
import { useFilteredData } from '../hooks';
import { styles } from '../styles';
import { Filters, HistoryRenderItem } from '../ui';

export const HistoryByMonthScreen = (props: any) => {
  const [tags, setTags] = useState<SpentTags[] | undefined>(undefined);
  const historyDate = props.route.params.params;

  const { filteredHistory } = useFilteredData(tags, historyDate);

  return (
    <UserLayout title="История трат" withBackButton>
      <FlatList
        contentContainerStyle={{ paddingVertical: 20 }}
        data={filteredHistory?.reverse()}
        ItemSeparatorComponent={ItemSeparator}
        keyExtractor={keyExtractorByMonth}
        renderItem={RenderItemByMonth}
      />

      <Filters activeFilters={tags} setFilters={setTags} />
    </UserLayout>
  );
};

const RenderItemByMonth = ({ item }: ListRenderItemInfo<HistoryItem>) => (
  <HistoryRenderItem.ByMonth item={item} />
);

const keyExtractorByMonth = (item: HistoryItem) =>
  item.name + item.date + item.value;

const ItemSeparator = () => <View style={styles.separator} />;
