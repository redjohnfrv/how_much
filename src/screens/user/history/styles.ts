import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  separator: {
    height: 24,
  },
});

export const light = StyleSheet.create({});

export const dark = StyleSheet.create({});
