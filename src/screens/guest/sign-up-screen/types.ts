import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { GuestStoriesStackParamListExtended } from 'navigation/guest/guest-stack';
import { APP_ROUTES } from 'navigation/routes';

export interface SignUnScreenProps {
  navigation: NativeStackNavigationProp<
    GuestStoriesStackParamListExtended,
    typeof APP_ROUTES.SIGN_UP_SCREEN
  >;
  route: RouteProp<
    GuestStoriesStackParamListExtended,
    typeof APP_ROUTES.SIGN_UP_SCREEN
  >;
}
