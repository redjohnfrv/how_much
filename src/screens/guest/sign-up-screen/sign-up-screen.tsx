import React, { FC, useCallback, useState } from 'react';
import { View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';

import { useFocusEffect } from '@react-navigation/native';
import { APP_ROUTES } from 'navigation/routes';
import { actions } from 'redux/actions';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';
import { useAppTheme } from 'resources/hooks';
import { Button } from 'ui';

import { PIN_LENGTH } from '../constants';
import { GuestLayout, Pin } from '../ui';
import { dark, light } from './styles';
import { SignUnScreenProps } from './types';

export const SignUpScreen: FC<SignUnScreenProps> = ({ navigation }) => {
  const theme = useAppTheme({ dark, light });
  const toast = useToast();
  const dispatch = useAppDispatch();
  const pin = useAppSelector(selectors.auth.pin);
  const [enteredPin, setEnteredPin] = useState<string>('');

  const handleSavePress = () => {
    if (pin) {
      toast.show('У вас уже есть PIN!', { type: 'error' });
      setEnteredPin('');
    } else {
      dispatch(actions.auth.setPin(enteredPin));
      toast.show('PIN создан!', { type: 'success' });
      navigation.navigate(APP_ROUTES.SIGN_IN_SCREEN);
    }
  };

  const handleSignUpPress = () => {
    navigation.navigate(APP_ROUTES.RESTORE_PIN_SCREEN);
  };

  useFocusEffect(
    useCallback(() => {
      return () => {
        setEnteredPin('');
      };
    }, []),
  );

  return (
    <GuestLayout>
      <Pin onNumberPress={setEnteredPin} pin={enteredPin} />
      <View style={theme.buttons}>
        <Button.Default
          disabled={enteredPin?.length < PIN_LENGTH}
          onPress={handleSavePress}
          style={theme.primaryButton}
          text="Сохранить"
          variant="primary"
        />
        <Button.Default
          onPress={handleSignUpPress}
          text="Восстановить PIN"
          variant="secondary"
        />
      </View>
    </GuestLayout>
  );
};
