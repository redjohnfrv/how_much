import React, { FC, useCallback, useState } from 'react';
import { View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';

import { useFocusEffect } from '@react-navigation/native';
import { APP_ROUTES } from 'navigation/routes';
import { actions } from 'redux/actions';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';
import { useAppTheme } from 'resources/hooks';
import { Button } from 'ui';

import { PIN_LENGTH } from '../constants';
import { GuestLayout, Pin } from '../ui';
import { dark, light } from './styles';
import { SignInScreenProps } from './types';

export const SignInScreen: FC<SignInScreenProps> = ({ navigation }) => {
  const theme = useAppTheme({ dark, light });
  const toast = useToast();
  const dispatch = useAppDispatch();
  const pin = useAppSelector(selectors.auth.pin);
  const [enteredPin, setEnteredPin] = useState<string>('');

  const handleSignInPress = () => {
    if (pin === enteredPin) {
      dispatch(actions.auth.signIn());
    } else {
      toast.show('Неверный PIN', { type: 'error' });
    }
  };

  const handleSignUpPress = () => {
    navigation.navigate(APP_ROUTES.SIGN_UP_SCREEN);
  };

  useFocusEffect(
    useCallback(() => {
      return () => {
        setEnteredPin('');
      };
    }, []),
  );

  return (
    <GuestLayout withBg>
      <Pin onNumberPress={setEnteredPin} pin={enteredPin} />
      <View style={theme.buttons}>
        <Button.Default
          disabled={enteredPin?.length < PIN_LENGTH}
          onPress={handleSignInPress}
          style={theme.primaryButton}
          text="Войти"
          variant="primary"
        />
        <Button.Default
          onPress={handleSignUpPress}
          text="Создать PIN"
          variant="secondary"
        />
      </View>
    </GuestLayout>
  );
};
