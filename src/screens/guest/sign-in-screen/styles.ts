import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  buttons: {
    marginTop: 'auto',
  },
  primaryButton: {
    marginBottom: 12,
  },
});

export const light = StyleSheet.create({
  buttons: {
    ...styles.buttons,
  },
  primaryButton: {
    ...styles.primaryButton,
  },
});

export const dark = StyleSheet.create({
  buttons: {
    ...styles.buttons,
  },
  primaryButton: {
    ...styles.primaryButton,
  },
});
