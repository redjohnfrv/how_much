import React, { FC } from 'react';
import { View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';

import { APP_ROUTES } from 'navigation/routes';
import { actions } from 'redux/actions';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectors } from 'redux/selectors';
import { useAppTheme } from 'resources/hooks';
import { Button } from 'ui';

import { GuestLayout } from '../ui';
import { dark, light } from './styles';
import { RestorePinScreenProps } from './types';
import { Description } from './ui';

export const RestorePinScreen: FC<RestorePinScreenProps> = ({ navigation }) => {
  const theme = useAppTheme({ dark, light });
  const dispatch = useAppDispatch();
  const toast = useToast();
  const pin = useAppSelector(selectors.auth.pin);

  const handleResetPress = () => {
    dispatch(actions.auth.reset());
    dispatch(actions.history.clearHistory());
    navigation.navigate(APP_ROUTES.SIGN_UP_SCREEN);
  };

  const handleRestorePress = () => {
    if (pin) {
      const ninjaPin = `${pin[0] + '**' + pin[3]}`;
      toast.show(`Ваш пин: ${ninjaPin}`, { type: 'success' });
      navigation.navigate(APP_ROUTES.SIGN_IN_SCREEN);
    }
  };

  return (
    <GuestLayout>
      <Description />
      <View style={theme.buttons}>
        <Button.Default
          onPress={handleResetPress}
          style={theme.primaryButton}
          text="Сбросить PIN"
          variant="primary"
        />
        <Button.Default
          disabled={Boolean(!pin)}
          onPress={handleRestorePress}
          text="Восстановить PIN"
          variant="secondary"
        />
      </View>
    </GuestLayout>
  );
};
