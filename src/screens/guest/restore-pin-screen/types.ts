import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { GuestStoriesStackParamListExtended } from 'navigation/guest/guest-stack';
import { APP_ROUTES } from 'navigation/routes';

export interface RestorePinScreenProps {
  navigation: NativeStackNavigationProp<
    GuestStoriesStackParamListExtended,
    typeof APP_ROUTES.RESTORE_PIN_SCREEN
  >;
  route: RouteProp<
    GuestStoriesStackParamListExtended,
    typeof APP_ROUTES.RESTORE_PIN_SCREEN
  >;
}
