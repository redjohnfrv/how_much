import React from 'react';
import { Image, ImageStyle, Text, View } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import logo from 'resources/images/logo/how-much-logo.png';

import { dark, light } from './styles';

export const Description = () => {
  const theme = useAppTheme({ dark, light });

  return (
    <View style={theme.root}>
      <View style={theme.wrap}>
        <Text style={theme.title}>Сбросить PIN</Text>
        <Text style={theme.text}>
          В случае, если Вы решили сбросить ваш PIN, вся информация вашего
          профиля How much будет удалена вы начнете с чистого листа!
        </Text>
      </View>
      <View style={theme.wrap}>
        <Text style={theme.title}>Восстановить PIN</Text>
        <Text style={theme.text}>
          Если вы хотите восстановить PIN, то вы получите уведомление внутри
          приложения How much, содержащее первую и последнюю цифры вашего PIN.
          Надеемся, это поможет :D
        </Text>
      </View>
      <View style={theme.logo}>
        <Image source={logo} style={theme.image as ImageStyle} />
      </View>
    </View>
  );
};
