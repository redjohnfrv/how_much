import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  image: {
    height: 144,
    width: 144,
  },
  logo: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  root: {
    flex: 1,
  },
  text: {
    ...typography.p1_regular_16,
  },
  title: {
    marginBottom: 12,
    ...typography.h1_bold_24,
  },
  wrap: {
    borderRadius: 12,
    marginTop: 16,
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
});

export const light = StyleSheet.create({
  image: {
    ...styles.image,
  },
  logo: {
    ...styles.logo,
  },
  root: {
    ...styles.root,
  },
  text: {
    ...styles.text,
    color: colors.light.grayscale[9],
  },
  title: {
    ...styles.title,
    color: colors.light.grayscale[9],
  },
  wrap: {
    ...styles.wrap,
    backgroundColor: colors.light.grayscale[5],
  },
});

export const dark = StyleSheet.create({
  image: {
    ...styles.image,
  },
  logo: {
    ...styles.logo,
  },
  root: {
    ...styles.root,
  },
  text: {
    ...styles.text,
    color: colors.dark.grayscale[3],
  },
  title: {
    ...styles.title,
    color: colors.dark.grayscale[3],
  },
  wrap: {
    ...styles.wrap,
    backgroundColor: colors.dark.grayscale[0],
  },
});
