import { FC, ReactNode, useMemo } from 'react';
import { Image, ImageStyle, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { useAppTheme } from 'resources/hooks';
import { authBg } from 'resources/images';
import { utils } from 'resources/utils';

import { dark, light } from './styles';

interface GuestLayoutProps {
  children: ReactNode;
  withBg?: boolean;
}

export const GuestLayout: FC<GuestLayoutProps> = ({
  children,
  withBg = false,
}) => {
  const theme = useAppTheme({ dark, light });
  const { getRandomFromArray } = utils;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const randomizedBg = useMemo(() => getRandomFromArray(authBg), []);

  return (
    <View style={theme.root}>
      {withBg && (
        <Image source={randomizedBg} style={theme.background as ImageStyle} />
      )}

      <SafeAreaView style={theme.safeArea}>{children}</SafeAreaView>
    </View>
  );
};
