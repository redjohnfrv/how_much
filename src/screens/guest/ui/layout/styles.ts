import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  background: {
    height: '100%',
    objectFit: 'cover',
    position: 'absolute',
    width: '100%',
  },
  root: {
    flex: 1,
    position: 'relative',
  },
  safeArea: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
});

export const light = StyleSheet.create({
  background: {
    ...styles.background,
  },
  root: {
    ...styles.root,
  },
  safeArea: {
    ...styles.safeArea,
  },
});

export const dark = StyleSheet.create({
  background: {
    ...styles.background,
  },
  root: {
    ...styles.root,
  },
  safeArea: {
    ...styles.safeArea,
  },
});
