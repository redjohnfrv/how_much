import React, { Dispatch, FC, SetStateAction } from 'react';
import { Pressable, Text, View } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { BackspaceIcon } from 'resources/icons';

import {
  PIN_LENGTH,
  asterix,
  numbersLine1,
  numbersLine2,
  numbersLine3,
} from '../../../constants';
import { dark, light } from './styles';

interface PinProps {
  onNumberPress: Dispatch<SetStateAction<string>>;
  pin: string;
}

export const Pin: FC<PinProps> = ({ onNumberPress, pin }) => {
  const theme = useAppTheme({ dark, light });

  const handleNumberPress = (value: string) => {
    onNumberPress(prev => {
      if (prev.length < PIN_LENGTH) {
        return prev + value;
      } else {
        return prev;
      }
    });
  };

  const handlePinClear = () => {
    onNumberPress(prev => {
      if (prev) {
        const lastIndex = prev.length - 1;

        return prev.slice(0, lastIndex);
      } else {
        return prev;
      }
    });
  };

  return (
    <View style={theme.root}>
      <Text style={theme.title}>Введите ваш PIN</Text>
      <View style={theme.pinContainer}>
        {Array.from({ length: PIN_LENGTH }).map((_, index) => {
          const hasValue = Boolean(pin[index]);
          return (
            <Text key={index} style={[theme.pin, hasValue && theme.pinVisible]}>
              {asterix}
            </Text>
          );
        })}
      </View>
      <View style={theme.numbersContainer}>
        <View style={theme.line}>
          {numbersLine1.map(item => (
            <Pressable
              key={item}
              onPress={() => handleNumberPress(item)}
              style={({ pressed }) => [
                theme.numberButton,
                pressed && theme.active,
              ]}>
              <Text style={theme.number}>{item}</Text>
            </Pressable>
          ))}
        </View>
        <View style={theme.line}>
          {numbersLine2.map(item => (
            <Pressable
              key={item}
              onPress={() => handleNumberPress(item)}
              style={({ pressed }) => [
                theme.numberButton,
                pressed && theme.active,
              ]}>
              <Text style={theme.number}>{item}</Text>
            </Pressable>
          ))}
        </View>
        <View style={theme.line}>
          {numbersLine3.map(item => (
            <Pressable
              key={item}
              onPress={() => handleNumberPress(item)}
              style={({ pressed }) => [
                theme.numberButton,
                pressed && theme.active,
              ]}>
              <Text style={theme.number}>{item}</Text>
            </Pressable>
          ))}
        </View>
        <View style={[theme.line, theme.lineReverse]}>
          <Pressable
            onPress={handlePinClear}
            style={({ pressed }) => [
              theme.numberButton,
              pressed && theme.active,
            ]}>
            <BackspaceIcon height={80} width={80} />
          </Pressable>
        </View>
      </View>
    </View>
  );
};
