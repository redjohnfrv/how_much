import { StyleSheet } from 'react-native';

import { sizes } from 'resources/styles/sizes';
import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  active: {
    borderRadius: 50,
  },
  line: {
    flexDirection: 'row',
  },
  lineReverse: {
    justifyContent: 'flex-end',
  },
  number: {
    ...typography.h1_bold_56,
  },
  numberButton: {
    alignItems: 'center',
    height: 100,
    justifyContent: 'center',
    width: 100,
  },
  numbersContainer: {
    alignSelf: 'center',
    backgroundColor: colors.light.effects.shadow[4],
    borderRadius: 60,
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginBottom: 40,
    marginTop: 'auto',
  },
  pin: {
    borderBottomWidth: 2,
    opacity: 0,
    ...typography.h1_bold_32,
  },
  pinContainer: {
    alignSelf: 'center',
    backgroundColor: colors.light.effects.shadow[4],
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 8,
    width: sizes.screen.smallSide / 2,
  },
  pinVisible: {
    opacity: 1,
  },
  root: {
    flex: 1,
  },
  title: {
    backgroundColor: colors.light.effects.shadow[4],
    borderRadius: 30,
    marginBottom: 20,
    textAlign: 'center',
    ...typography.h1_bold_32,
  },
});

export const light = StyleSheet.create({
  active: {
    backgroundColor: colors.light.effects.shadow[4],
    ...styles.active,
  },
  line: {
    ...styles.line,
  },
  lineReverse: {
    ...styles.lineReverse,
  },
  number: {
    color: colors.light.grayscale[1],
    ...styles.number,
  },
  numberButton: {
    ...styles.numberButton,
  },
  numbersContainer: {
    ...styles.numbersContainer,
  },
  pin: {
    borderBottomColor: colors.light.grayscale[1],
    color: colors.light.grayscale[1],
    ...styles.pin,
  },
  pinContainer: {
    ...styles.pinContainer,
  },
  pinVisible: {
    ...styles.pinVisible,
  },
  root: {
    ...styles.root,
  },
  title: {
    color: colors.light.grayscale[1],
    ...styles.title,
  },
});

export const dark = StyleSheet.create({
  active: {
    backgroundColor: colors.dark.effects.shadow[4],
    ...styles.active,
  },
  line: {
    ...styles.line,
  },
  lineReverse: {
    ...styles.lineReverse,
  },
  number: {
    color: colors.dark.grayscale[1],
    ...styles.number,
  },
  numberButton: {
    ...styles.numberButton,
  },
  numbersContainer: {
    ...styles.numbersContainer,
  },
  pin: {
    borderBottomColor: colors.dark.grayscale[1],
    color: colors.dark.grayscale[1],
    ...styles.pin,
  },
  pinContainer: {
    ...styles.pinContainer,
  },
  pinVisible: {
    ...styles.pinVisible,
  },
  root: {
    ...styles.root,
  },
  title: {
    color: colors.dark.grayscale[1],
    ...styles.title,
  },
});
