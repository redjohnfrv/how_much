export const PIN_LENGTH = 4;
export const asterix = '*';
export const numbersLine1 = ['1', '2', '3'];
export const numbersLine2 = ['4', '5', '6'];
export const numbersLine3 = ['7', '8', '9'];
