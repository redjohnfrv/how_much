import React, { useMemo } from 'react';
import RNBootSplash from 'react-native-bootsplash';

import { NavigationContainer } from '@react-navigation/native';
import { useAppSelector } from 'redux/hooks';
import { useAppThemeType } from 'resources/hooks';

import { selectors } from '../redux/selectors';
import { GuestStack } from './guest';
import { GuestStackParamsList } from './guest/guest-stack';
import { NAVIGATION_CONTAINER_THEME } from './options';
import { APP_ROUTES } from './routes';
import { SubNavigator } from './types';
import { BottomStack, BottomStackParamsList } from './user/stacks';

export const RootStack: React.FC = () => {
  const themeType = useAppThemeType();
  const theme = NAVIGATION_CONTAINER_THEME[themeType];
  const isAuth = useAppSelector(selectors.auth.isAuth);

  const stackNavigation = useMemo(() => {
    if (isAuth) {
      return <BottomStack />;
    }

    return <GuestStack />;
  }, [isAuth]);

  return (
    <NavigationContainer
      onReady={() => RNBootSplash.hide({ fade: true })}
      theme={theme}>
      {stackNavigation}
    </NavigationContainer>
  );
};

export type RootStackParamList = {
  [APP_ROUTES.GUEST_STACK]: SubNavigator<GuestStackParamsList>;
  [APP_ROUTES.USER_STACK]: SubNavigator<BottomStackParamsList>;
};
