import { DefaultTheme, Theme } from '@react-navigation/native';
import { colors } from 'resources/styles/themes';

export const DEFAULT_SCREEN_OPTIONS = {
  cardShadowEnabled: true,
  headerShown: false,
};

export const NAVIGATION_CONTAINER_THEME: Record<'dark' | 'light', Theme> = {
  dark: {
    colors: {
      ...DefaultTheme.colors,
      background: colors.dark.background[3],
    },
    dark: true,
  },
  light: {
    colors: {
      ...DefaultTheme.colors,
      background: colors.light.background[6],
    },
    dark: false,
  },
};
