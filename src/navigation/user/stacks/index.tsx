import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationProp } from '@react-navigation/native';
import { RootStackParamList } from 'navigation';
import { DEFAULT_SCREEN_OPTIONS } from 'navigation/options';
import { APP_ROUTES } from 'navigation/routes';
import { HistoryIcon, HomeIcon, SettingsIcon } from 'resources/icons';

import { BottomBar } from '../ui';
import { HistoryStack } from './history';
import { HomeStack } from './home';
import { SettingsStack } from './settings';

const Tab = createBottomTabNavigator<BottomStackParamsList>();

export const BottomStack: React.FC = () => {
  return (
    <Tab.Navigator
      initialRouteName={APP_ROUTES.HOME_STACK}
      screenOptions={DEFAULT_SCREEN_OPTIONS}
      tabBar={props => <BottomBar {...props} />}>
      <Tab.Screen
        component={HomeStack}
        name={APP_ROUTES.HOME_STACK}
        options={{
          tabBarIcon: ({ focused, size }) => {
            return <HomeIcon focused={focused} height={size} width={size} />;
          },
        }}
      />
      <Tab.Screen
        component={HistoryStack}
        name={APP_ROUTES.HISTORY_STACK}
        options={{
          tabBarIcon: ({ focused, size }) => {
            return <HistoryIcon focused={focused} height={size} width={size} />;
          },
        }}
      />
      <Tab.Screen
        component={SettingsStack}
        name={APP_ROUTES.SETTINGS_STACK}
        options={{
          tabBarIcon: ({ focused, size }) => {
            return (
              <SettingsIcon focused={focused} height={size} width={size} />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export type BottomStackNavigationProp = NavigationProp<BottomStackParamsList>;

export type UserStoriesStackParamListExtended = RootStackParamList &
  BottomStackParamsList;

export type BottomStackParamsList = {
  [APP_ROUTES.HISTORY_STACK]: undefined;
  [APP_ROUTES.HOME_STACK]: undefined;
  [APP_ROUTES.SETTINGS_STACK]: undefined;
};
