import React from 'react';

import { NavigationProp, RouteProp } from '@react-navigation/native';
import {
  NativeStackNavigationProp,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';
import { RootStackParamList } from 'navigation';
import { DEFAULT_SCREEN_OPTIONS } from 'navigation/options';
import { APP_ROUTES } from 'navigation/routes';
import {
  HistorySettingsScreen,
  ReleaseNotesScreen,
  SettingsScreen,
} from 'screens';

import { BottomStackParamsList } from '../index';

const Stack =
  createNativeStackNavigator<SettingsStoriesStackParamListExtended>();

export const SettingsStack: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName={APP_ROUTES.SETTINGS_SCREEN}>
      <Stack.Screen
        component={SettingsScreen}
        name={APP_ROUTES.SETTINGS_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
      <Stack.Screen
        component={HistorySettingsScreen}
        name={APP_ROUTES.HISTORY_SETTINGS_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
      <Stack.Screen
        component={ReleaseNotesScreen}
        name={APP_ROUTES.RELEASE_NOTES_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
    </Stack.Navigator>
  );
};

export type SettingsStoriesStackParamListExtended = RootStackParamList &
  SettingsStackParamsList;

export type SettingsStackNavigationProp =
  NavigationProp<SettingsStackParamsList>;

export type SettingsStackParamsList = {
  [APP_ROUTES.HISTORY_SETTINGS_SCREEN]: undefined;
  [APP_ROUTES.RELEASE_NOTES_SCREEN]: undefined;
  [APP_ROUTES.SETTINGS_SCREEN]: undefined;
};

export interface SettingsStackProps {
  navigation: NativeStackNavigationProp<
    BottomStackParamsList,
    typeof APP_ROUTES.SETTINGS_STACK
  >;
  route: RouteProp<BottomStackParamsList, typeof APP_ROUTES.SETTINGS_STACK>;
}
