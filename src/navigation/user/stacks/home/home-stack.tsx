import React from 'react';

import { RouteProp } from '@react-navigation/native';
import {
  NativeStackNavigationProp,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';
import { RootStackParamList } from 'navigation';
import { DEFAULT_SCREEN_OPTIONS } from 'navigation/options';
import { APP_ROUTES } from 'navigation/routes';
import { HomeScreen } from 'screens';

import { BottomStackParamsList } from '../index';

const Stack = createNativeStackNavigator<HomeStoriesStackParamListExtended>();

export const HomeStack: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName={APP_ROUTES.HOME_SCREEN}>
      <Stack.Screen
        component={HomeScreen}
        name={APP_ROUTES.HOME_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
    </Stack.Navigator>
  );
};

export type HomeStoriesStackParamListExtended = RootStackParamList &
  HomeStackParamsList;

export type HomeStackParamsList = {
  [APP_ROUTES.HOME_SCREEN]: undefined;
};

export interface HomeStackProps {
  navigation: NativeStackNavigationProp<
    BottomStackParamsList,
    typeof APP_ROUTES.HOME_STACK
  >;
  route: RouteProp<BottomStackParamsList, typeof APP_ROUTES.HOME_STACK>;
}
