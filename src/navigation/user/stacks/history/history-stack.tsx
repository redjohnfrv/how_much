import React from 'react';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList } from 'navigation';
import { DEFAULT_SCREEN_OPTIONS } from 'navigation/options';
import { APP_ROUTES } from 'navigation/routes';
import { HistoryByMonthScreen, HistoryScreen } from 'screens';

import { BottomStackParamsList } from '../index';

const Stack =
  createNativeStackNavigator<HistoryStoriesStackParamListExtended>();

export const HistoryStack: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName={APP_ROUTES.HISTORY_SCREEN}>
      <Stack.Screen
        component={HistoryScreen}
        name={APP_ROUTES.HISTORY_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
      <Stack.Screen
        component={HistoryByMonthScreen}
        name={APP_ROUTES.HISTORY_BY_MONTH_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
    </Stack.Navigator>
  );
};

export type HistoryStoriesStackParamListExtended = RootStackParamList &
  HistoryStackParamsList;

export type HistoryStackParamsList = {
  [APP_ROUTES.HISTORY_BY_MONTH_SCREEN]: {
    params: string;
  };
  [APP_ROUTES.HISTORY_SCREEN]: undefined;
};
