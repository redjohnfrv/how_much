import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { BottomTabBarProps } from '@react-navigation/bottom-tabs/src/types';
import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

export const BottomBar: React.FC<BottomTabBarProps> = ({
  descriptors,
  navigation,
  state,
}) => {
  const theme = useAppTheme({ dark, light });
  const { bottom } = useSafeAreaInsets();

  return (
    <View style={[theme.root, { paddingBottom: bottom + 15 }]}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];

        const tabBarIcon = options.tabBarIcon;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            canPreventDefault: true,
            target: route.key,
            type: 'tabPress',
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, { merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            target: route.key,
            type: 'tabLongPress',
          });
        };

        return (
          <TouchableOpacity
            key={route.key}
            onLongPress={onLongPress}
            onPress={onPress}
            style={theme.item}>
            {tabBarIcon && (
              <>
                {tabBarIcon({
                  color: '',
                  focused: isFocused,
                  size: 26,
                })}
              </>
            )}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
