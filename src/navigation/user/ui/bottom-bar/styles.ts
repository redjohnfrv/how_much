import { StyleSheet } from 'react-native';

import { sizes } from 'resources/styles/sizes';
import { colors } from 'resources/styles/themes';

export const styles = StyleSheet.create({
  item: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  root: {
    alignItems: 'center',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 50,
    paddingTop: 20,
    position: 'absolute',
    width: sizes.screen.smallSide,
  },
});

export const light = StyleSheet.create({
  item: {
    ...styles.item,
  },
  root: {
    backgroundColor: colors.light.background[5],
    elevation: 15,
    ...styles.root,
  },
});

export const dark = StyleSheet.create({
  item: {
    ...styles.item,
  },
  root: {
    backgroundColor: colors.dark.background[4],
    elevation: 15,
    ...styles.root,
  },
});
