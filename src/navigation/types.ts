import { ParamListBase } from '@react-navigation/native';

export type SubNavigator<T extends ParamListBase> = {
  [K in keyof T]: { params?: T[K]; screen: K };
}[keyof T];
