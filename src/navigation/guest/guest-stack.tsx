import React from 'react';

import { RouteProp } from '@react-navigation/native';
import {
  NativeStackNavigationProp,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';
import { RootStackParamList } from 'navigation';
import { RestorePinScreen, SignInScreen, SignUpScreen } from 'screens';

import { DEFAULT_SCREEN_OPTIONS } from '../options';
import { APP_ROUTES } from '../routes';

const Stack = createNativeStackNavigator<GuestStoriesStackParamListExtended>();

export const GuestStack: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName={APP_ROUTES.SIGN_IN_SCREEN}>
      <Stack.Screen
        component={SignInScreen}
        name={APP_ROUTES.SIGN_IN_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
      <Stack.Screen
        component={RestorePinScreen}
        name={APP_ROUTES.RESTORE_PIN_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
      <Stack.Screen
        component={SignUpScreen}
        name={APP_ROUTES.SIGN_UP_SCREEN}
        options={DEFAULT_SCREEN_OPTIONS}
      />
    </Stack.Navigator>
  );
};

export type GuestStoriesStackParamListExtended = RootStackParamList &
  GuestStackParamsList;

export type GuestStackParamsList = {
  [APP_ROUTES.RESTORE_PIN_SCREEN]: undefined;
  [APP_ROUTES.SIGN_IN_SCREEN]: undefined;
  [APP_ROUTES.SIGN_UP_SCREEN]: undefined;
};

export interface GuestStackProps {
  navigation: NativeStackNavigationProp<
    RootStackParamList,
    typeof APP_ROUTES.GUEST_STACK
  >;
  route: RouteProp<RootStackParamList, typeof APP_ROUTES.GUEST_STACK>;
}
