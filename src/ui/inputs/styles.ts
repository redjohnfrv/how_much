import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  error: {
    ...typography.p2_light_16,
  },
  input: {
    borderRadius: 22,
    borderWidth: 1,
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 8,
    ...typography.p1_regular_16,
  },
  inputContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 3,
    paddingBottom: 4,
  },
});

export const light = (focused: boolean, error: string | undefined) =>
  StyleSheet.create({
    error: {
      ...styles.error,
      color: colors.light.additional.warning_error,
    },
    input: {
      backgroundColor: error
        ? colors.light.additional.warning_error_20
        : colors.light.grayscale[0],
      borderColor:
        (error && colors.light.additional.warning_error) ||
        (!error && focused && colors.light.grayscale[6]) ||
        colors.light.grayscale[4],
      color: colors.light.grayscale[10],
      ...styles.input,
    },
    inputContainer: {
      ...styles.inputContainer,
    },
  });

export const dark = (focused: boolean, error: string | undefined) =>
  StyleSheet.create({
    error: {
      color: colors.dark.additional.warning_error,
      ...styles.error,
    },
    input: {
      backgroundColor: error
        ? colors.dark.additional.warning_error_20
        : colors.dark.grayscale[0],
      borderColor:
        (error && colors.dark.additional.warning_error) ||
        (!error && focused && colors.dark.grayscale[4]) ||
        colors.dark.grayscale[6],
      color: colors.dark.grayscale[10],
      ...styles.input,
    },
    inputContainer: {
      ...styles.inputContainer,
    },
  });
