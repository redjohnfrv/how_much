import React, { useState } from 'react';
import {
  KeyboardTypeOptions,
  Text,
  TextInput,
  View,
  ViewStyle,
} from 'react-native';

const MAX_INPUT_LENGTH = 30;

import { useAppTheme, useAppThemeType } from 'resources/hooks';
import { colors } from 'resources/styles/themes';

import { dark, light } from './styles';

interface DefaultInputProps {
  error?: string;
  keyboardType?: KeyboardTypeOptions;
  maxLength?: number;
  onChange: (value: string) => void;
  placeholder: string;
  required?: boolean;
  style?: ViewStyle;
  value?: string;
}

export const DefaultInput: React.FC<DefaultInputProps> = ({
  error,
  keyboardType,
  maxLength = MAX_INPUT_LENGTH,
  onChange,
  placeholder,
  style,
  value,
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const theme = useAppTheme({ dark, light });
  const currentTheme = useAppThemeType();
  const isLightTheme = currentTheme === 'light';
  const styled = typeof theme === 'function' ? theme(isFocused, error) : theme;

  const { placeholderTextColor, selectionColor } = {
    placeholderTextColor: isLightTheme
      ? colors.light.grayscale[6]
      : colors.dark.grayscale[6],
    selectionColor: isLightTheme
      ? colors.light.grayscale[7]
      : colors.dark.grayscale[1],
  };

  return (
    <View style={style}>
      <View style={styled.inputContainer}>
        <TextInput
          keyboardType={keyboardType}
          maxLength={maxLength}
          onBlur={() => setIsFocused(false)}
          onChangeText={onChange}
          onFocus={() => setIsFocused(true)}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          selectionColor={selectionColor}
          style={styled.input}
          value={value}
        />
      </View>
      {error && (
        <Text numberOfLines={1} style={styled.error}>
          {error}
        </Text>
      )}
    </View>
  );
};
