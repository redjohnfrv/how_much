import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';

import { typography } from '../../resources/styles/typography';

export const styles = StyleSheet.create({
  root: {
    borderRadius: 12,
    flexWrap: 'nowrap',
    paddingHorizontal: 8,
    paddingVertical: 6,
  },
  text: {
    ...typography.p3_bold_18,
  },
});

export const light = {
  active: {
    backgroundColor: colors.light.primary[2],
  },
  root: {
    ...styles.root,
    backgroundColor: colors.light.primary[3],
  },
  text: {
    ...styles.text,
    color: colors.light.grayscale[0],
  },
};

export const dark = {
  active: {
    backgroundColor: colors.dark.primary[3],
  },
  root: {
    ...styles.root,
    backgroundColor: colors.dark.primary[4],
  },
  text: {
    ...styles.text,
    color: colors.dark.grayscale[1],
  },
};
