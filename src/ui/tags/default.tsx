import { Pressable, Text } from 'react-native';

import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

interface DefaultTagProps {
  isActive: boolean;
  onPress: () => void;
  tag: string;
}

export const DefaultTag = ({ isActive, onPress, tag }: DefaultTagProps) => {
  const theme = useAppTheme({ dark, light });

  return (
    <Pressable
      onPress={onPress}
      style={() => [theme.root, isActive && theme.active]}>
      <Text style={theme.text}>{tag}</Text>
    </Pressable>
  );
};
