import { FC, ReactNode } from 'react';
import { Pressable, PressableProps, Text, View } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Style } from 'resources/types';
import { Modal } from 'ui';

import { dark, light } from '../styles';
import { Variant } from '../types';

interface ButtonWithConfirmationProps extends PressableProps {
  content: ReactNode;
  isModalVisible: boolean;
  onClose: () => void;
  onConfirm: () => void;
  style?: Style;
  text: string;
  title?: string;
  variant?: Variant;
}

export const ButtonWithConfirmation: FC<ButtonWithConfirmationProps> = ({
  content,
  disabled,
  isModalVisible,
  onClose,
  onConfirm,
  style,
  text,
  title,
  variant = 'positive',
  ...props
}) => {
  const theme = useAppTheme({ dark, light });

  const colors = {
    negative: {
      background: theme.negativeBackground,
      disabled: theme.negativeBackground,
      pressed: theme.negativePressed,
      text: theme.negativeText,
      textDisabled: theme.negativeText,
    },
    positive: {
      background: theme.positiveBackground,
      disabled: theme.positiveDisabled,
      pressed: theme.positivePressed,
      text: theme.positiveText,
      textDisabled: theme.positiveDisabled,
    },
    primary: {
      background: theme.primaryBackground,
      disabled: theme.primaryDisabled,
      pressed: theme.primaryPressed,
      text: theme.primaryText,
      textDisabled: theme.primaryTextDisabled,
    },
    secondary: {
      background: theme.secondaryBackground,
      disabled: theme.secondaryBackground,
      pressed: theme.secondaryPressed,
      text: theme.secondaryText,
      textDisabled: theme.secondaryText,
    },
  };

  return (
    <View>
      <Pressable
        {...props}
        pointerEvents={disabled ? 'none' : undefined}
        style={({ pressed }) => [
          [
            theme.root,
            pressed ? colors[variant].pressed : colors[variant].background,
            disabled && colors[variant].disabled,
            style,
          ],
        ]}>
        <Text
          style={[
            theme.text,
            colors[variant].text,
            disabled && colors[variant].textDisabled,
          ]}>
          {text}
        </Text>

        <Modal.Default
          onClose={onClose}
          onConfirm={onConfirm}
          title={title}
          visible={isModalVisible}>
          {content}
        </Modal.Default>
      </Pressable>
    </View>
  );
};
