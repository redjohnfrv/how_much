export type Variant = 'negative' | 'positive' | 'primary' | 'secondary';
