import { FC } from 'react';
import { Pressable, PressableProps, Text } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Style } from 'resources/types';

import { dark, light } from './styles';
import { Variant } from './types';

interface SmallButtonProps extends PressableProps {
  style?: Style;
  text: string;
  variant?: Variant;
}

export const SmallButton: FC<SmallButtonProps> = ({
  disabled,
  style,
  text,
  variant = 'primary',
  ...props
}) => {
  const theme = useAppTheme({ dark, light });

  const colors = {
    primary: {
      background: theme.primaryBackground,
      disabled: theme.primaryDisabled,
      pressed: theme.primaryPressed,
      text: theme.primaryText,
      textDisabled: theme.primaryTextDisabled,
    },
    secondary: {
      background: theme.secondaryBackground,
      disabled: theme.secondaryBackground,
      pressed: theme.secondaryPressed,
      text: theme.secondaryText,
      textDisabled: theme.secondaryText,
    },
  };

  return (
    <Pressable
      {...props}
      pointerEvents={disabled ? 'none' : undefined}
      style={({ pressed }) => [
        [
          theme.root,
          pressed ? colors[variant].pressed : colors[variant].background,
          disabled && colors[variant].disabled,
          style,
        ],
      ]}>
      <Text
        numberOfLines={1}
        style={[
          theme.text,
          colors[variant].text,
          disabled && colors[variant].textDisabled,
        ]}>
        {text}
      </Text>
    </Pressable>
  );
};
