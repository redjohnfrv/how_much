import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  root: {
    borderRadius: 12,
    maxWidth: 130,
    minWidth: 80,
    paddingHorizontal: 16,
    paddingVertical: 9,
  },
  text: {
    textAlign: 'center',
    ...typography.p3_bold_18,
  },
});

export const light = StyleSheet.create({
  primaryBackground: {
    backgroundColor: colors.light.primary[3],
  },
  primaryDisabled: {
    backgroundColor: colors.light.grayscale[4],
  },
  primaryPressed: {
    backgroundColor: colors.light.grayscale[5],
  },
  primaryText: {
    color: colors.light.primary[1],
  },
  primaryTextDisabled: {
    color: colors.light.grayscale[5],
  },
  root: {
    ...styles.root,
  },
  secondaryBackground: {
    backgroundColor: colors.light.grayscale[6],
  },
  secondaryPressed: {
    backgroundColor: colors.light.grayscale[7],
  },
  secondaryText: {
    color: colors.light.grayscale[0],
  },
  text: {
    ...styles.text,
  },
});

export const dark = StyleSheet.create({
  primaryBackground: {
    backgroundColor: colors.dark.primary[2],
  },
  primaryDisabled: {
    backgroundColor: colors.dark.grayscale[7],
  },
  primaryPressed: {
    backgroundColor: colors.dark.grayscale[7],
  },
  primaryText: {
    color: colors.dark.grayscale[3],
  },
  primaryTextDisabled: {
    color: colors.light.grayscale[7],
  },
  root: {
    ...styles.root,
  },
  secondaryBackground: {
    backgroundColor: colors.dark.grayscale[6],
  },
  secondaryPressed: {
    backgroundColor: colors.dark.grayscale[7],
  },
  secondaryText: {
    color: colors.dark.grayscale[1],
  },
  text: {
    ...styles.text,
  },
});
