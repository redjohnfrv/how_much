import React from 'react';
import { Pressable, Text, View } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { ArrowRightIcon } from 'resources/icons';

import { dark, light } from './styles';

interface NavigationArrowButtonProps {
  onPressed: () => void;
  title: string;
}

export const NavigationArrowButton = ({
  onPressed,
  title,
}: NavigationArrowButtonProps) => {
  const theme = useAppTheme({ dark, light });

  return (
    <Pressable
      onPress={onPressed}
      style={({ pressed }) => [theme.button, pressed && theme.buttonPressed]}>
      <Text style={theme.buttonText}>{title}</Text>
      <View style={theme.iconWrapper}>
        <ArrowRightIcon
          color={theme.icon.backgroundColor}
          height={20}
          width={20}
        />
      </View>
    </Pressable>
  );
};
