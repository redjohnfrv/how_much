import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: 18,
    flexDirection: 'row',
    height: 36,
    justifyContent: 'center',
    marginHorizontal: 12,
    paddingHorizontal: 16,
  },
  buttonText: {
    ...typography.p1_regular_16,
  },
  iconWrapper: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 15,
    height: 30,
    justifyContent: 'center',
    marginLeft: 9,
    transform: [{ translateY: 2 }],
    width: 30,
  },
});

export const light = StyleSheet.create({
  button: {
    ...styles.button,
  },
  buttonPressed: {
    backgroundColor: colors.light.grayscale[2],
  },
  buttonText: {
    ...styles.buttonText,
    color: colors.light.primary[1],
  },
  icon: {
    backgroundColor: colors.light.primary[1],
  },
  iconWrapper: {
    ...styles.iconWrapper,
    backgroundColor: colors.light.grayscale[2],
  },
});

export const dark = StyleSheet.create({
  button: {
    ...styles.button,
  },
  buttonPressed: {
    backgroundColor: colors.dark.grayscale[8],
  },
  buttonText: {
    ...styles.buttonText,
    color: colors.dark.primary[1],
  },
  icon: {
    backgroundColor: colors.dark.primary[1],
  },
  iconWrapper: {
    ...styles.iconWrapper,
    backgroundColor: colors.dark.grayscale[8],
  },
});
