import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';

import { typography } from '../../resources/styles/typography';

const styles = StyleSheet.create({
  backdrop: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20,
    width: '100%',
  },
  centeredView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  children: {
    minWidth: 268,
  },
  indicator: {
    height: 4,
    width: 64,
  },
  modalView: {
    alignItems: 'center',
    borderRadius: 20,
    elevation: 5,
    padding: 16,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    width: 300,
  },
  title: {
    ...typography.h1_bold_24,
  },
});

export const light = StyleSheet.create({
  backdrop: {
    ...styles.backdrop,
    backgroundColor: colors.light.opacity_50,
  },
  background: {
    backgroundColor: colors.light.background[1],
  },
  buttons: {
    ...styles.buttons,
  },
  centeredView: {
    ...styles.centeredView,
    backgroundColor: colors.light.opacity_50,
  },
  children: {
    ...styles.children,
  },
  indicator: {
    ...styles.indicator,
    backgroundColor: colors.light.grayscale[4],
  },
  modalView: {
    ...styles.modalView,
    backgroundColor: colors.light.grayscale[1],
    shadowColor: '#000',
  },
  title: {
    ...styles.title,
    color: colors.light.primary[1],
  },
});

export const dark = StyleSheet.create({
  backdrop: {
    ...styles.backdrop,
    backgroundColor: colors.dark.opacity_50,
  },
  background: {
    backgroundColor: colors.dark.background[1],
  },
  buttons: {
    ...styles.buttons,
  },
  centeredView: {
    ...styles.centeredView,
    backgroundColor: colors.dark.opacity_50,
  },
  children: {
    ...styles.children,
  },
  indicator: {
    ...styles.indicator,
    backgroundColor: colors.dark.grayscale[8],
  },
  modalView: {
    ...styles.modalView,
    backgroundColor: colors.dark.primary[1],
    shadowColor: '#000',
  },
  title: {
    ...styles.title,
    color: colors.dark.primary[3],
  },
});
