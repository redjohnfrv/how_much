import React, { useRef } from 'react';
import {
  GestureResponderEvent,
  Modal,
  ModalProps,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Button } from 'ui';

import { dark, light } from './styles';

interface DefaultModalProps extends ModalProps {
  cancelButtonText?: string;
  confirmButtonText?: string;
  onClose?: () => void;
  onConfirm: () => void;
  title?: string;
}

export const DefaultModal = (props: DefaultModalProps) => {
  const {
    cancelButtonText = 'Отмена',
    children,
    confirmButtonText = 'Да',
    onClose,
    onConfirm,
    title,
  } = props;
  const theme = useAppTheme({ dark, light });

  const contentRef = useRef<View>(null);

  const handleClickOutside = (e: GestureResponderEvent) => {
    // @ts-ignore TS2367
    if (contentRef.current && e.target !== contentRef.current) {
      onClose?.();
    }
  };

  const handleConfirm = () => {
    onConfirm?.();
    onClose?.();
  };

  return (
    <Modal animationType="fade" transparent {...props}>
      <TouchableWithoutFeedback onPress={handleClickOutside}>
        <View style={theme.centeredView}>
          <View ref={contentRef} style={theme.modalView}>
            {Boolean(title) && <Text style={theme.title}>{title}</Text>}

            <View style={theme.children}>{children}</View>

            <View style={theme.buttons}>
              <Button.Small onPress={handleConfirm} text={confirmButtonText} />

              {Boolean(onClose) && (
                <Button.Small
                  onPress={onClose}
                  text={cancelButtonText}
                  variant="secondary"
                />
              )}
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
