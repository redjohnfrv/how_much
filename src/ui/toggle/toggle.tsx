import React from 'react';
import { Switch, ViewStyle } from 'react-native';

import { useAppThemeType } from 'resources/hooks';
import { colors } from 'resources/styles/themes';

interface ToggleProps {
  checked: boolean;
  disabled?: boolean;
  onClick: () => void;
  style?: ViewStyle;
}

export const Toggle: React.FC<ToggleProps> = ({
  checked,
  disabled,
  onClick,
  style,
}) => {
  const themeType = useAppThemeType();

  const themeColors = {
    dark: {
      thumb: disabled ? colors.dark.grayscale[6] : colors.dark.primary[3],
      trackFalse: disabled ? colors.dark.grayscale[3] : colors.dark.primary[1],
      trackTrue: disabled ? colors.dark.grayscale[3] : colors.dark.primary[1],
    },
    light: {
      thumb: disabled ? colors.light.grayscale[0] : colors.light.grayscale[0],
      trackFalse: disabled
        ? colors.light.grayscale[3]
        : colors.light.primary[3],
      trackTrue: disabled ? colors.light.grayscale[3] : colors.light.primary[3],
    },
  };

  return (
    <Switch
      disabled={disabled}
      onValueChange={onClick}
      style={style}
      thumbColor={themeColors[themeType].thumb}
      trackColor={{
        false: themeColors[themeType].trackFalse,
        true: themeColors[themeType].trackTrue,
      }}
      value={checked}
    />
  );
};
