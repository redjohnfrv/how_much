import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    borderRadius: 12,
    paddingHorizontal: 12,
    paddingVertical: 12,
    width: '100%',
  },
});

export const light = StyleSheet.create({
  root: {
    ...styles.root,
    backgroundColor: colors.light.grayscale[5],
  },
});

export const dark = StyleSheet.create({
  root: {
    ...styles.root,
    backgroundColor: colors.dark.grayscale[0],
  },
});
