import { ReactNode } from 'react';
import { View, ViewProps } from 'react-native';

import { useAppTheme } from 'resources/hooks';
import { Style } from 'resources/types';

import { dark, light } from './styles';

interface DefaultSurfaceProps extends ViewProps {
  children: ReactNode;
  styles?: Style;
}

export const DefaultSurface = ({ children, ...props }: DefaultSurfaceProps) => {
  const theme = useAppTheme({ dark, light });

  return (
    <View style={theme.root} {...props}>
      {children}
    </View>
  );
};
