import { authSelectors } from './reducers/auth';
import { historySelectors } from './reducers/history';
import { settingsSelectors } from './reducers/settings';

export const selectors = {
  auth: authSelectors,
  history: historySelectors,
  settings: settingsSelectors,
};
