import { authActions } from './reducers/auth';
import { historyActions } from './reducers/history';
import { settingsActions } from './reducers/settings';

export const actions = {
  auth: {
    ...authActions,
  },
  history: {
    ...historyActions,
  },
  settings: {
    ...settingsActions,
  },
};
