import { PayloadAction, createSlice } from '@reduxjs/toolkit';

type State = {
  isAuth: boolean;
  pin: null | string;
};

const initialState: State = {
  isAuth: false,
  pin: null,
};

const authSlice = createSlice({
  initialState,
  name: 'auth',
  reducers: {
    reset: () => initialState,
    setPin: (state, action: PayloadAction<string>) => ({
      ...state,
      pin: action.payload,
    }),
    signIn: state => ({
      ...state,
      isAuth: true,
    }),
    signOut: state => ({ ...state, isAuth: false }),
  },
});

export const { actions, reducer } = authSlice;
