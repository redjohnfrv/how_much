import { RootState } from '../../types';

export const pin = (state: RootState) => state.auth.pin;
export const isAuth = (state: RootState) => state.auth.isAuth;
