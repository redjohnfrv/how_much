import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { ThemeType } from 'resources/styles/themes/types';

type State = {
  theme: ThemeType;
};

const initialState: State = {
  theme: 'system',
};

const settingsSlice = createSlice({
  initialState,
  name: 'settings',
  reducers: {
    changeColorTheme(state, { payload }: PayloadAction<ThemeType>) {
      state.theme = payload;
    },
  },
});

export const { actions, reducer } = settingsSlice;
