export * as settingsSelectors from './selectors';
export {
  actions as settingsActions,
  reducer as settingsReducer,
} from './settingsSlice';
