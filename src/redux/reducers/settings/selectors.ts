import { RootState } from '../../types';

export const theme = (state: RootState) => state.settings.theme;
