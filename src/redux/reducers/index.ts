import { combineReducers } from '@reduxjs/toolkit';

import { authReducer } from './auth';
import { historyReducer } from './history';
import { settingsReducer } from './settings';

export const appReducer = combineReducers({
  auth: authReducer,
  history: historyReducer,
  settings: settingsReducer,
});
