export {
  actions as historyActions,
  reducer as historyReducer,
} from './historySlice';
export * as historySelectors from './selectors';
