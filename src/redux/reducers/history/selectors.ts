import { RootState } from '../../types';

export const history = (state: RootState) => state.history.history;
export const prevTotal = (state: RootState) => state.history.prevTotal;
