import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { dateAdapter } from 'resources/utils/date-adapter';

import { HistoryItem, TotalObject } from './types';

type State = {
  history: HistoryItem[];
  prevTotal: TotalObject;
};

const initialState: State = {
  history: [],
  prevTotal: {},
};

const historySlice = createSlice({
  initialState,
  name: 'history',
  reducers: {
    clearHistory: state => {
      state.history = initialState.history;
    },
    deepClearHistory: () => initialState,
    restoreHistory: (
      _,
      action: PayloadAction<{
        history: HistoryItem[];
      }>,
    ) => {
      const prevTotal: TotalObject = {};

      action.payload?.history?.forEach(item => {
        const itemDate = dateAdapter(item.date);

        if (prevTotal.hasOwnProperty(itemDate)) {
          prevTotal[itemDate] = String(
            Number(prevTotal[itemDate]) + Number(item.value),
          );
        } else {
          Object.defineProperty(prevTotal, itemDate, {
            configurable: true,
            enumerable: true,
            value: item.value,
            writable: true,
          });
        }
      });

      return {
        history: action.payload.history,
        prevTotal,
      };
    },
    updateHistory: (state, action: PayloadAction<HistoryItem>) => {
      const dateForPrevTotals = dateAdapter(action.payload.date);
      const prevTotalPayload = state.prevTotal[dateForPrevTotals]
        ? String(
            Number(state.prevTotal[dateForPrevTotals]) +
              Number(action.payload.value),
          )
        : action.payload.value;

      return {
        ...state,
        history: [...state.history, action.payload],
        prevTotal: {
          ...state.prevTotal,
          [dateForPrevTotals]: prevTotalPayload,
        },
      };
    },
  },
});

export const { actions, reducer } = historySlice;
