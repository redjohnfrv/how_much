export enum SpentTags {
  ALCOHOL = 'ALCOHOL',
  ANIMALS = 'ANIMALS',
  ANYTHING = 'ANYTHING',
  CAFE = 'CAFE',
  CLOTHES = 'CLOTHES',
  COFFEE = 'COFFEE',
  ELECTRONICS = 'ELECTRONICS',
  FOOD = 'FOOD',
  GAMES = 'GAMES',
  GIFTS_AND_CELEBRATING = 'GIFTS_AND_CELEBRATING',
  HEALTH = 'HEALTH',
  HOME_STUFF = 'HOME_STUFF',
  TAXI = 'TAXI',
  TRANSFERS = 'TRANSFERS',
  TRIPS = 'TRIPS',
}
export const spentTagsAdapter: Record<SpentTags, string> = {
  ALCOHOL: 'Алкоголь',
  ANIMALS: 'Домашние животные',
  ANYTHING: 'Разное',
  CAFE: 'Кафе и заведения',
  CLOTHES: 'Одежда',
  COFFEE: 'Кофе',
  ELECTRONICS: 'Техника',
  FOOD: 'Еда',
  GAMES: 'Игры и настолки',
  GIFTS_AND_CELEBRATING: 'Подарки',
  HEALTH: 'Здоровье',
  HOME_STUFF: 'Для дома',
  TAXI: 'Такси',
  TRANSFERS: 'Переводы',
  TRIPS: 'Путешествия',
};

export type HistoryItem = {
  date: string;
  name: string;
  tag?: keyof typeof SpentTags;
  value: string;
};

export type HistoryTotalItem = {
  [key: string]: string;
};

export type TotalObject = Record<string, string>;
