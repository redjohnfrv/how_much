import React, { PropsWithChildren } from 'react';

import { PersistGate } from 'redux-persist/integration/react';

import { persistor } from '../../store';

export const Persist: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <PersistGate loading={null} persistor={persistor}>
      {children}
    </PersistGate>
  );
};
