import { Persist } from './persist';
import { Store } from './store';

const ReduxProviders = { Persist, Store };

export { ReduxProviders };
