import AsyncStorage from '@react-native-async-storage/async-storage';
import { Middleware, configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  persistReducer,
  persistStore,
} from 'redux-persist';
import { IS_DEVELOP } from 'resources/constants';

import { appReducer } from './reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth', 'history', 'settings'],
};

const persistedReducer = persistReducer(persistConfig, appReducer);

const middlewares: Middleware[] = [];

if (IS_DEVELOP) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

export const store = configureStore({
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(middlewares),
  reducer: persistedReducer,
});

setupListeners(store.dispatch);

export const persistor = persistStore(store);
