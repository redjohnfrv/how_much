import React, { PropsWithChildren } from 'react';
import { ToastProvider } from 'react-native-toast-notifications';

import { Toast } from './components';

export const Toaster: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <ToastProvider
      animationDuration={250}
      duration={3000}
      placement="top"
      renderToast={Toast}
      swipeEnabled={true}>
      {children}
    </ToastProvider>
  );
};
