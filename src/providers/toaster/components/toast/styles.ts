import { StyleSheet } from 'react-native';

import { colors } from 'resources/styles/themes';
import { typography } from 'resources/styles/typography';

export const styles = StyleSheet.create({
  message: {
    ...typography.p2_light_16,
    maxWidth: '90%',
  },
  root: {
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingVertical: 10,
    width: '100%',
  },
});

export const light = StyleSheet.create({
  errorContainer: {
    backgroundColor: colors.light.additional.warning_error,
  },
  message: {
    ...styles.message,
    color: colors.light.grayscale[1],
  },
  root: {
    ...styles.root,
  },
  successContainer: {
    backgroundColor: colors.light.additional.success_dark,
  },
});

export const dark = StyleSheet.create({
  errorContainer: {
    backgroundColor: colors.dark.additional.warning_error,
  },
  message: {
    ...styles.message,
    color: colors.dark.grayscale[0],
  },
  root: {
    ...styles.root,
  },
  successContainer: {
    backgroundColor: colors.dark.additional.success,
  },
});
