import React from 'react';
import { Text, View } from 'react-native';

import { ToastProps } from 'react-native-toast-notifications/lib/typescript/toast';
import { useAppTheme } from 'resources/hooks';

import { dark, light } from './styles';

export const Toast = ({ message, type }: ToastProps) => {
  const isSuccess = type === 'success';
  const isError = type === 'error';

  const styles = useAppTheme({ dark, light });

  return (
    <View
      style={[
        styles.root,
        isError && styles.errorContainer,
        isSuccess && styles.successContainer,
      ]}>
      <Text style={styles.message}>{message}</Text>
    </View>
  );
};
