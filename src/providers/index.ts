import { GestureHandler } from './gesture-handler';
import { Toaster } from './toaster';

export const Providers = {
  GestureHandler,
  Toaster,
};
