import React, { PropsWithChildren } from 'react';
import { StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

export const GestureHandler: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <GestureHandlerRootView style={styles.root}>
      {children}
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});
