import { SafeAreaProvider } from 'react-native-safe-area-context';

import { RootStack } from 'navigation';
import { Providers } from 'providers';
import { ReduxProviders } from 'redux/providers';

export const App = () => {
  return (
    <SafeAreaProvider>
      <ReduxProviders.Store>
        <ReduxProviders.Persist>
          <Providers.GestureHandler>
            <Providers.Toaster>
              <RootStack />
            </Providers.Toaster>
          </Providers.GestureHandler>
        </ReduxProviders.Persist>
      </ReduxProviders.Store>
    </SafeAreaProvider>
  );
};
