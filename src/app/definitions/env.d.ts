declare module '@env' {
  export const API_BASE_URL: string;
  export const NODE_ENV: string;
  export const ANDROID_KEYSTORE_PASSWORD: string;
  export const ANDROID_KEY_PASSWORD: string;
}
