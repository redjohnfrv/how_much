import { SvgProps } from 'react-native-svg';

export interface SvgPropsWithFocus extends SvgProps {
  focused: boolean;
}
