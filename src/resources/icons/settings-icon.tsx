import * as React from 'react';
import Svg, { Path, Circle } from 'react-native-svg';
import { SvgPropsWithFocus } from './types';
import { useAppThemeType } from 'resources/hooks';
import { colors } from 'resources/styles/themes';

const SvgSettingsIcon = ({ focused, ...props }: SvgPropsWithFocus) => {
  const themeType = useAppThemeType();
  const color = {
    light: focused ? colors.light.primary[2] : colors.light.grayscale[5],
    dark: focused ? colors.dark.primary[1] : colors.dark.grayscale[5],
  };

  return (
    <Svg fill="none" viewBox="0 0 24 24" {...props}>
      <Path
        stroke={color[themeType]}
        strokeWidth={2}
        d="M10.521 3.624a2 2 0 0 1 2.958 0l.735.807a2 2 0 0 0 1.572.651l1.091-.05a2 2 0 0 1 2.091 2.09l-.05 1.092a2 2 0 0 0 .65 1.572l.808.735a2 2 0 0 1 0 2.958l-.807.735a2 2 0 0 0-.651 1.572l.05 1.091a2 2 0 0 1-2.09 2.091l-1.092-.05a2 2 0 0 0-1.572.65l-.735.808a2 2 0 0 1-2.958 0l-.735-.807a2 2 0 0 0-1.572-.651l-1.091.05a2 2 0 0 1-2.092-2.09l.051-1.092a2 2 0 0 0-.65-1.572l-.808-.735a2 2 0 0 1 0-2.958l.807-.735a2 2 0 0 0 .651-1.572l-.05-1.091a2 2 0 0 1 2.09-2.092l1.092.051a2 2 0 0 0 1.572-.65l.735-.808Z"
      />
      <Circle cx={12} cy={12} r={3} stroke={color[themeType]} strokeWidth={2} />
    </Svg>
  );
};
export default SvgSettingsIcon;
