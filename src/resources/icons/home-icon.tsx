import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { SvgPropsWithFocus } from './types';
import { colors } from '../styles/themes';
import { useAppThemeType } from 'resources/hooks';

const SvgHomeIcon = ({ focused, ...props }: SvgPropsWithFocus) => {
  const themeType = useAppThemeType();
  const color = {
    light: focused ? colors.light.primary[2] : colors.light.grayscale[5],
    dark: focused ? colors.dark.primary[1] : colors.dark.grayscale[5],
  };

  return (
    <Svg fill="none" viewBox="0 0 24 24" {...props}>
      <Path
        fill={color[themeType]}
        fillRule="evenodd"
        d="M14.4 3.2a1 1 0 0 1 1.4.2L18.5 7h.003a3 3 0 0 1 2.965 3.456l-1.077 7A3 3 0 0 1 17.426 20H6.574a3 3 0 0 1-2.965-2.544l-1.077-7A3 3 0 0 1 5.497 7H5.5l2.7-3.6a1 1 0 1 1 1.6 1.2L8 7h8l-1.8-2.4a1 1 0 0 1 .2-1.4ZM5.988 9h12.515a1 1 0 0 1 .988 1.152l-1.076 7a1 1 0 0 1-.989.848H6.574a1 1 0 0 1-.989-.848l-1.077-7A1 1 0 0 1 5.497 9h.491Z"
        clipRule="evenodd"
      />
    </Svg>
  );
};
export default SvgHomeIcon;
