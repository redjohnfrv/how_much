import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { SvgPropsWithFocus } from './types';
import { useAppThemeType } from 'resources/hooks';
import { colors } from 'resources/styles/themes';

const SvgHistoryIcon = ({ focused, ...props }: SvgPropsWithFocus) => {
  const themeType = useAppThemeType();
  const color = {
    light: focused ? colors.light.primary[2] : colors.light.grayscale[5],
    dark: focused ? colors.dark.primary[1] : colors.dark.grayscale[5],
  };

  return (
    <Svg fill="none" viewBox="0 0 24 24" {...props}>
      <Path
        fill={color[themeType]}
        d="M7 5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-3.5a1 1 0 1 1 2 0V16a5 5 0 0 1-5 5H7a5 5 0 0 1-5-5V8a5 5 0 0 1 5-5h3.5a1 1 0 1 1 0 2H7Z"
      />
      <Path
        fill={color[themeType]}
        fillRule="evenodd"
        d="M18.843 3.586a2 2 0 0 0-2.828 0L11.68 7.92l-.666 4.025a1.1 1.1 0 0 0 1.266 1.265l4.02-.67 4.334-4.334a2 2 0 0 0 0-2.829l-1.792-1.791Zm-5.65 7.444.356-2.15L17.429 5l1.792 1.792-3.881 3.88-2.147.358Z"
        clipRule="evenodd"
      />
    </Svg>
  );
};
export default SvgHistoryIcon;
