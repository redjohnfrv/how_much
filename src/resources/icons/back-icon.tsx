import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
const SvgBackIcon = ({ color = '#000', ...props }: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    {...props}>
    <Path
      fill={color}
      d="M8.423 7.522a.75.75 0 0 0-1.078-1.044l1.078 1.044ZM4.46 9.454a.75.75 0 1 0 1.078 1.044L4.46 9.454Zm1.061-.016a.75.75 0 0 0-1.044 1.076l1.044-1.076Zm1.84 3.876a.75.75 0 0 0 1.044-1.076l-1.044 1.076ZM5 9.226a.75.75 0 0 0 0 1.5v-1.5Zm10.62.75.015-.75a.8.8 0 0 0-.015 0v.75ZM19 13.488l-.75-.014v.028l.75-.014ZM15.62 17v.75h.015L15.62 17ZM5 16.25a.75.75 0 0 0 0 1.5v-1.5Zm2.345-9.772L4.461 9.454l1.078 1.044 2.884-2.976-1.078-1.044Zm-2.867 4.036 2.884 2.8 1.044-1.076-2.884-2.8-1.044 1.076Zm.522.212h10.62v-1.5H5v1.5Zm10.605 0a2.698 2.698 0 0 1 2.645 2.748l1.5.028a4.198 4.198 0 0 0-4.115-4.276l-.03 1.5Zm2.645 2.776a2.698 2.698 0 0 1-2.645 2.748l.03 1.5a4.198 4.198 0 0 0 4.115-4.276l-1.5.028Zm-2.63 2.748H5v1.5h10.62v-1.5Z"
    />
  </Svg>
);
export default SvgBackIcon;
