export { default as ArrowRightIcon } from './arrow-right-icon';
export { default as BackIcon } from './back-icon';
export { default as BackspaceIcon } from './backspace-icon';
export { default as FiltersIcon } from './filters-icon';
export { default as HistoryIcon } from './history-icon';
export { default as HomeIcon } from './home-icon';
export { default as SettingsIcon } from './settings-icon';
export { default as TrashIcon } from './trash-icon';
