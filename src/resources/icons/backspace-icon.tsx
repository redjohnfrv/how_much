import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';
import { useAppThemeType } from '../hooks';
import { colors } from '../styles/themes';

const SvgBackspaceIcon = ({ ...props }: SvgProps) => {
  const themeType = useAppThemeType();
  const color = {
    light: colors.light.grayscale[1],
    dark: colors.dark.grayscale[1],
  };

  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 -0.5 25 25"
      {...props}>
      <Path
        stroke={color[themeType]}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1.5}
        d="m5.91 12.665 2.446 2.861c.24.294.596.467.975.474l3.72-.007h3.188a3.242 3.242 0 0 0 3.261-3.22v-2.55A3.242 3.242 0 0 0 16.24 7l-6.909.007c-.379.006-.735.18-.975.473L5.91 10.342a1.85 1.85 0 0 0 0 2.323v0Z"
        clipRule="evenodd"
      />
      <Path
        fill={color[themeType]}
        d="M12.16 9.464a.75.75 0 0 0-1.048 1.072l1.048-1.072Zm.487 2.572a.75.75 0 0 0 1.048-1.072l-1.048 1.072Zm1.05-1.071a.75.75 0 1 0-1.051 1.07l1.05-1.07Zm.478 2.57a.75.75 0 0 0 1.05-1.07l-1.05 1.07Zm-.48-1.499a.75.75 0 0 0-1.048-1.072l1.048 1.072Zm-2.583.428a.75.75 0 1 0 1.048 1.072l-1.048-1.072Zm1.534-1.5a.75.75 0 0 0 1.05 1.071l-1.05-1.07Zm2.58-.429a.75.75 0 0 0-1.051-1.07l1.05 1.07Zm-4.114.001 1.535 1.5 1.048-1.072-1.535-1.5-1.048 1.072Zm1.534 1.5 1.529 1.5 1.05-1.071-1.529-1.5-1.05 1.07Zm0-1.072-1.534 1.5 1.048 1.072 1.535-1.5-1.048-1.072Zm1.05 1.071 1.53-1.5-1.051-1.07-1.53 1.5 1.051 1.07Z"
      />
    </Svg>
  );
};
export default SvgBackspaceIcon;
