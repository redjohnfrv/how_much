import { ImageStyle, TextStyle, ViewStyle, useColorScheme } from 'react-native';
import { useSelector } from 'react-redux';

import { selectors } from 'redux/selectors';

type NamedStyles<T> = { [P in keyof T]: ImageStyle | TextStyle | ViewStyle };

interface Params<T> {
  dark: NamedStyles<T>;
  light: NamedStyles<T>;
}

export const useAppTheme = <T>({ dark, light }: Params<T>) => {
  const storeTheme = useSelector(selectors.settings.theme);
  const systemTheme = useColorScheme();

  if (storeTheme === 'system') {
    return systemTheme === 'dark' ? dark : light;
  }

  return storeTheme === 'dark' ? dark : light;
};
