import { useColorScheme } from 'react-native';
import { useSelector } from 'react-redux';

import { selectors } from 'redux/selectors';

export const useAppThemeType = () => {
  const storeTheme = useSelector(selectors.settings.theme);
  const systemTheme = useColorScheme();

  if (storeTheme === 'system') {
    return systemTheme === 'dark' ? 'dark' : 'light';
  }

  return storeTheme === 'dark' ? 'dark' : 'light';
};
