import { useCallback, useMemo, useState } from 'react';

type ModalState = {
  closeModal: () => void;
  isVisible: boolean;
  openModal: () => void;
};

interface UseModalState {
  onClose?: () => void;
  onOpen?: () => void;
}

export const useModalState = ({ onClose, onOpen }: UseModalState) => {
  const [isVisible, setVisible] = useState(false);

  const openModal = useCallback(() => {
    onOpen?.();
    setVisible(true);
  }, [onOpen]);

  const closeModal = useCallback(() => {
    onClose?.();
    setVisible(false);
  }, [onClose]);

  const modalState: ModalState = useMemo(
    () => ({
      closeModal,
      isVisible,
      openModal,
    }),
    [closeModal, isVisible, openModal],
  );

  return modalState;
};
