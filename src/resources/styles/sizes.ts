import { Dimensions } from 'react-native';

const { height, width } = Dimensions.get('screen');

const deviceScreenHeight = Dimensions.get('screen').height;
const deviceScreenWidth = Dimensions.get('screen').width;

const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;
const deviceHeight =
  deviceScreenWidth < deviceScreenHeight
    ? deviceScreenHeight
    : deviceScreenWidth;

const ratioDeviceHeightToScreenHeight = deviceHeight / screenHeight;

export const sizes = {
  ratioDeviceHeightToScreenHeight,
  screen: {
    bigSide: screenHeight,
    smallSide: screenWidth,
  },
};
