export const Z_INDEX_VARIABLES = {
  zIndexDecorActiveEl: 2,
  zIndexDropdown: 99,
  zIndexFixedEl: 1000,
  zIndexHidden: -1,
  zIndexOverlay: 9999,
  zIndexStickyEl: 200,
};
