import { StyleSheet } from 'react-native';

export const FONT_BOLD = 'intelone-mono-font-family-bold';
export const FONT_LIGHT = 'intelone-mono-font-family-light';
export const FONT_REGULAR = 'intelone-mono-font-family-regular';

export const typography = StyleSheet.create({
  h1_bold_24: {
    fontFamily: FONT_BOLD,
    fontSize: 24,
    lineHeight: 32,
  },
  h1_bold_32: {
    fontFamily: FONT_BOLD,
    fontSize: 32,
    lineHeight: 40,
  },
  h1_bold_56: {
    fontFamily: FONT_BOLD,
    fontSize: 56,
    lineHeight: 62,
  },
  p1_regular_16: {
    fontFamily: FONT_REGULAR,
    fontSize: 16,
    lineHeight: 24,
  },
  p2_light_16: {
    fontFamily: FONT_LIGHT,
    fontSize: 16,
    lineHeight: 24,
  },
  p3_bold_18: {
    fontFamily: FONT_BOLD,
    fontSize: 18,
    lineHeight: 26,
  },
  p4_regular_24: {
    fontFamily: FONT_REGULAR,
    fontSize: 24,
    lineHeight: 24,
  },
  p5_regular_36: {
    fontFamily: FONT_REGULAR,
    fontSize: 36,
    lineHeight: 44,
  },
});
