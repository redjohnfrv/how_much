type TotalObj = {
  [key: string]: string;
};

export const getTargetMonth = (obj: TotalObj, substring: string) => {
  for (let key in obj) {
    if (key.includes(substring)) {
      return obj[key];
    }
  }
  return undefined;
};
