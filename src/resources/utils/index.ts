import { dateAdapter } from './date-adapter';
import { getFormattedDate } from './get-formatted-date';
import { getNormalizedDateValue } from './get-normalized-date-value';
import { getNormalizedValue } from './get-normalized-value';
import { getRandomFromArray } from './get-random-from-array';
import { getTargetMonth } from './get-target-month';
import { sortHistoryByDate } from './sort-history-by-date';
import { validator } from './validation';

export const utils = {
  dateAdapter,
  getFormattedDate,
  getNormalizedDateValue,
  getNormalizedValue,
  getRandomFromArray,
  getTargetMonth,
  sortHistoryByDate,
  validator,
};
