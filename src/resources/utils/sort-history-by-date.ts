import { HistoryItem } from 'redux/reducers/history/types';

export const sortHistoryByDate = (data: HistoryItem[]) =>
  data.sort((prev, next) => {
    const datePrev = prev.date;
    const dateNext = next.date;

    if (datePrev < dateNext) {
      return -1;
    }

    if (datePrev > dateNext) {
      return 1;
    }

    return 0;
  });
