export const getNormalizedDateValue = (value?: string) => {
  const isValueCorrect = value && !value?.includes('.');

  if (isValueCorrect) {
    if (value.length === 6) {
      const formattedValue = value
        .split(/(\d{2})/)
        .filter(Boolean)
        .join('.');

      return formattedValue;
    }
  }

  return value;
};
