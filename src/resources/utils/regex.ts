const numbers = /^[0-9.]+$/;
const digits = /^\d+$/;
const replaceDigits = /[^0-9]/g;
const divideByTwo = /^(\d{2})(\d{2})(\d{2})$/;

export const regex = {
  digits,
  divideByTwo,
  numbers,
  replaceDigits,
};
