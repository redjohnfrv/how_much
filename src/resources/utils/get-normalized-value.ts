import { regex } from './regex';

export const getNormalizedValue = (value: string | undefined) => {
  if (!value) {
    return;
  }

  const isNumericString = value.match(regex.digits);

  if (isNumericString) {
    return value;
  } else {
    const normalizedString = value.replace(regex.replaceDigits, '');

    return normalizedString;
  }
};
