import { isDate } from './is-date';
import { isEmptyString } from './is-empty-string';

export const validator = {
  isDate,
  isEmptyString,
};
