export const isEmptyString = (value?: string) => {
  return value && value.trim() ? undefined : 'Это обязательное поле!';
};
