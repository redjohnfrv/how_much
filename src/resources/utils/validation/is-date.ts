import { DATE_INPUT_LENGTH } from 'resources/constants';

import { regex } from '../regex';

export const isDate = (value: string | undefined) => {
  if (!value) {
    return undefined;
  }

  const isValid =
    value && regex.numbers.test(value) && value?.length == DATE_INPUT_LENGTH;

  if (!isValid) {
    return 'Введите дату правильно!';
  }
};
