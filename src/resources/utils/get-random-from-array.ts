export function getRandomFromArray<T>(arr: T[]) {
  const max = arr.length;
  const randomIndex = Math.floor(Math.random() * max);

  return arr[randomIndex];
}
