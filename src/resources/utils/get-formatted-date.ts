import moment from 'moment';

import { DATE_FORMAT } from '../constants';

export const getFormattedDate = (date?: string) => {
  return moment(date).format(DATE_FORMAT);
};
