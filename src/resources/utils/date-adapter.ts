import { numToMonthAdapter } from '../constants';

export const dateAdapter = (date: string) => {
  const monthAndYear = date.slice(3);
  const month = numToMonthAdapter[monthAndYear.slice(0, 2)];
  const normalizedDate = `${month} ${date.slice(6)}`;

  return normalizedDate;
};
