import { ImageStyle, ViewStyle } from 'react-native';

export type Style = ImageStyle | ImageStyle[] | ViewStyle | ViewStyle[];
