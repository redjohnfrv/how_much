import { NODE_ENV } from '@env';

export const IS_DEVELOP = NODE_ENV === 'develop';
export const DATE_INPUT_LENGTH = 8;

export const numToMonthAdapter: Record<string, string> = {
  '01': 'Январь',
  '02': 'Февраль',
  '03': 'Март',
  '04': 'Апрель',
  '05': 'Май',
  '06': 'Июнь',
  '07': 'Июль',
  '08': 'Август',
  '09': 'Сентябрь',
  '10': 'Октябрь',
  '11': 'Ноябрь',
  '12': 'Декабрь',
};

export const monthToNumAdapter: Record<string, string> = {
  Август: '08',
  Апрель: '04',
  Декабрь: '12',
  Июль: '07',
  Июнь: '06',
  Май: '05',
  Март: '03',
  Ноябрь: '11',
  Октябрь: '10',
  Сентябрь: '09',
  Февраль: '02',
  Январь: '01',
};

export const DATE_FORMAT = 'DD.MM.YY';
