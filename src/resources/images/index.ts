import bg1 from './bg-1.jpeg';
import bg2 from './bg-2.png';
import bg3 from './bg-3.jpeg';
import bg4 from './bg-4.jpeg';
import bg5 from './bg-5.jpeg';

export const authBg = [bg1, bg2, bg3, bg4, bg5];
